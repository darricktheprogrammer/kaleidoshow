import pytest
from pathlib import Path

from kaleidoshow.photoset import Photoset, ShuffledPhotoset


@pytest.fixture
def ordered_photoset():
    filelist = [
        Path("file_001.jpg"),
        Path("file_002.jpg"),
        Path("file_003.jpg"),
        Path("file_004.jpg"),
        Path("file_005.jpg"),
        Path("file_006.jpg"),
        Path("file_007.jpg"),
        Path("file_008.jpg"),
    ]
    return Photoset("Ordered Test Photoset", "/no/path", filelist)


@pytest.fixture
def shuffled_photoset():
    def _shuffled_photoset():
        filelist = [
            Path("file_001.jpg"),
            Path("file_002.jpg"),
            Path("file_003.jpg"),
            Path("file_004.jpg"),
            Path("file_005.jpg"),
            Path("file_006.jpg"),
            Path("file_007.jpg"),
            Path("file_008.jpg"),
        ]
        return ShuffledPhotoset("Shuffled Test Photoset", "/no/path", filelist)

    # https://docs.pytest.org/en/6.2.x/fixture.html#factories-as-fixtures
    return _shuffled_photoset


def test_Photoset_NewOrderedPhotoset_InitializesCorrectValues(ordered_photoset):
    photoset = ordered_photoset
    assert photoset.name == "Ordered Test Photoset"
    assert len(photoset) == 8
    assert photoset.current_file.name == "file_001.jpg"
    assert photoset.root_directory == "/no/path"
    assert photoset.mode == "ordered"


def test_ShuffledPhotoset_InitializesCorrectValues(shuffled_photoset):
    photosets = [shuffled_photoset() for _ in range(2)]
    assert not photosets[0] == photosets[1]
    assert len(photosets[0]) == 8
    assert photosets[0].mode == "shuffled"


def test_Photoset_Next_StartingFromBeginning_ReturnsFilesInOrder(ordered_photoset):
    photoset = ordered_photoset
    curr_file = photoset.next()
    assert curr_file.name == "file_002.jpg"

    curr_file = photoset.next()
    assert curr_file.name == "file_003.jpg"

    curr_file = photoset.next()
    assert curr_file.name == "file_004.jpg"


def test_Photoset_Next_AtEnd_LoopsToBeginning(ordered_photoset):
    photoset = ordered_photoset
    for i in range(5):
        # Don't need to check all items. Move toward the end of the photoset.
        photoset.next()

    curr_file = photoset.next()
    assert curr_file.name == "file_007.jpg"

    curr_file = photoset.next()
    assert curr_file.name == "file_008.jpg"

    curr_file = photoset.next()
    assert curr_file.name == "file_001.jpg"


def test_Photoset_Previous_StartingFromEnd_ReturnsFilesInReverseOrder(ordered_photoset):
    photoset = ordered_photoset
    for i in range(8):
        # Move to end of the photoset
        photoset.next()

    curr_file = photoset.previous()
    assert curr_file.name == "file_008.jpg"

    curr_file = photoset.previous()
    assert curr_file.name == "file_007.jpg"

    curr_file = photoset.previous()
    assert curr_file.name == "file_006.jpg"


def test_Photoset_Previous_AtBeginning_LoopsToEnd(ordered_photoset):
    photoset = ordered_photoset
    curr_file = photoset.next()
    assert curr_file.name == "file_002.jpg"

    curr_file = photoset.previous()
    assert curr_file.name == "file_001.jpg"

    curr_file = photoset.previous()
    assert curr_file.name == "file_008.jpg"


def test_Photoset_ToList_ReturnsFilesAsList(ordered_photoset):
    photoset = ordered_photoset
    expected_list = [
        Path("file_001.jpg"),
        Path("file_002.jpg"),
        Path("file_003.jpg"),
        Path("file_004.jpg"),
        Path("file_005.jpg"),
        Path("file_006.jpg"),
        Path("file_007.jpg"),
        Path("file_008.jpg"),
    ]
    photoset_list = list(photoset)
    assert type(photoset_list) == list
    assert photoset_list == expected_list


def test_ShuffledPhotoset_MovingForward_ReturnsShuffledFiles(shuffled_photoset):
    photoset = shuffled_photoset()

    # This is a test implentation detail. I know that the original input is in
    # alphabetical order.
    shuffled = list(photoset)
    assert shuffled != sorted(shuffled)


def test_ShuffledPhotoset_Reshuffle_ReshufflesTheFileList(shuffled_photoset):
    photoset = shuffled_photoset()

    # This is a test implentation detail. I know that the original input is in
    # alphabetical order.
    original = list(photoset)
    photoset.reshuffle()
    reshuffled = list(photoset)
    assert original != reshuffled


def test_ShuffledPhotoset_ConvertToOrdered_ReturnsOrderedPhotoset(shuffled_photoset):
    photoset = shuffled_photoset()

    ordered_photoset = photoset.convert_to_ordered_photoset()
    assert type(ordered_photoset) == Photoset
    assert photoset != ordered_photoset


def test_OrderedPhotoset_ConvertToShuffled_ReturnsShuffledPhotoset(ordered_photoset):
    photoset = ordered_photoset

    shuffled_photoset = photoset.convert_to_shuffled_photoset()
    assert type(shuffled_photoset) == ShuffledPhotoset
    assert photoset != shuffled_photoset


def test_ShuffledPhotoset_ConvertToOrdered_SetsCurrentImageToSame(shuffled_photoset):
    photoset = shuffled_photoset()
    photoset.next()
    current = photoset.next()
    ordered_photoset = photoset.convert_to_ordered_photoset()
    ordered_photoset.next()
    current_ordered = ordered_photoset.previous()
    assert current == current_ordered
