from pathlib import Path
from tempfile import TemporaryDirectory

import pytest

from kaleidoshow.app import Kaleidoshow


# Testing initialization is complicated. You must destroy any existing
# QApplication instance before the running the next test. This means there must
# be a guaranteed `QApplication.shutdown` call at the end of each test.
# However, with the way pytest fixtures work, it is impossible to encapsulate
# that shutdown in a fixture and have the ability to create a new instance with
# arguments. That means that every test requiring a new Kaleidoshow instance
# must call shutdown manually, taking care that it is still called even if any
# asserts in the test fail.
#
# Due to this, as much functionality as possible should be extracted into their
# own methods in the Kaleidoshow class and tested independently. Try to keep
# __init__ testing to a minimum.


def test_Kaleidoshow_Startup_LoadsDefaultConfiguration():
    app = Kaleidoshow()
    try:
        assert app.config.carousel_settings.interval == 30
    except RuntimeError:
        pass
    finally:
        app.shutdown()


def test_Kaleidoshow_Startup_OverwritesDefaultsWithExtras(test_root):
    extra_config = test_root / "test-config-files/basic-config.yml"
    app = Kaleidoshow(extra_configs=[extra_config])
    try:
        assert app.config.carousel_settings.interval == 15
    except RuntimeError:
        pass
    finally:
        app.shutdown()


def test_Kaleidoshow_Startup_GivenIgnoreKnownConfigsTrue_IngoresKnownConfigs(caplog):
    # This all happens during app startup and the app doesn't save any public
    # attributes to check against. For testing without using private
    # attributes, we have to check the log and see what activity Kaleidoshow is
    # reporting.
    with caplog.at_level("INFO"):
        app = Kaleidoshow(ignore_known_configs=True)
        try:
            assert "--ignore-known-configs: True" in caplog.text
            assert "Loading configuration from known files" not in caplog.text
        except RuntimeError:
            pass
        finally:
            app.shutdown()


def test_Kaleidoshow_Startup_ReadsCarouselsFromConfig(test_root):
    carousel_config = (
        "carousels:\n"
        f"  - {test_root}/test-images\n"
        "  -\n"
        f"    - {test_root}/test-images/carousel\n"
        f"    - {test_root}/test-images/kaleidoscope\n"
        )

    # NamedTemporaryFile would be good here, but it cannot be re-opened while
    # still open on Windows and is not gauranteed to exist outside of the
    # `with` statement
    with TemporaryDirectory() as tempdir:
        tempconfig = Path(tempdir) / "config.yml"
        with open(tempconfig, "w+") as configfile:
            configfile.write(carousel_config)
            configfile.seek(0)
        app = Kaleidoshow(ignore_known_configs=True, extra_configs=[str(tempconfig)])
    try:
        assert len(app.saved_carousels) == 2
        assert app.saved_carousels[0].name == "test-images"
        assert app.saved_carousels[1].name == "carousel | kaleidoscope"
    except RuntimeError:
        pass
    finally:
        app.shutdown()
