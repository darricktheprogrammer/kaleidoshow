from kaleidoshow.photoset import PhotosetLoader


def test_PhotosetLoader_LoadDirectory_OnlyImages_ReturnsAllImages(test_root):
    img_dir = test_root / "test-images/carousel"
    photoset = PhotosetLoader().load_directory(img_dir)
    assert photoset.name == "carousel"
    assert len(photoset) == 5


def test_PhotosetLoader_LoadDirectory_Ordered_ReturnsImagesAlphabetically(test_root):
    img_dir = test_root / "test-images/carousel"
    photoset = PhotosetLoader().load_directory(img_dir)
    assert photoset.mode == "ordered"
    assert photoset.current_file.name == "640px-Ross_Park_Carousel_5.jpg"
    assert photoset.previous().name == "Greenway_Carousel_-_Rose_Kennedy_Greenway_-_Boston,_MA_-_DSC05445.JPG"


def test_PhotosetLoader_LoadDirectory_Ordered_ReturnsImagesInNumericalOrder(test_root):
    img_dir = test_root / "test-images/number-files"
    photoset = PhotosetLoader().load_directory(img_dir)
    assert photoset.mode == "ordered"
    files = list(photoset)
    assert files[0].name == "1.jpg"
    assert files[1].name == "2.JPG"
    assert files[2].name == "1000.jpg"
    assert files[3].name == "2200.jpg"
    assert files[4].name == "55000.png"


def test_PhotosetLoader_LoadDirectory_Shuffled_ReturnsImagesShuffled(test_root):
    img_dir = test_root / "test-images/carousel"
    ordered_photoset = PhotosetLoader().load_directory(img_dir)

    # With such a small test set, occasionally shuffling files return them in
    # the original order. By comparing 3 ShuffledPhotosets, it is much more
    # likely to get the correct assertion.
    #
    # The test is repeated for the amount of times it took me to consistently
    # recreate a false positive with only 2 ShuffledPhotosets.
    ordered_files = list(ordered_photoset)
    for _ in range(300):
        shuffled_1 = PhotosetLoader().load_directory(img_dir, shuffled=True)
        shuffled_2 = PhotosetLoader().load_directory(img_dir, shuffled=True)
        assert ordered_files != list(shuffled_1) or ordered_files != list(shuffled_2)


def test_PhotosetLoader_LoadDirectory_ImagesMixedWithOtherFiles_ReturnsOnlyImages(test_root):
    img_dir = test_root / "test-images/kaleidoscope"
    photoset = PhotosetLoader().load_directory(img_dir)
    assert len(photoset) == 5


def test_PhotosetLoader_LoadDirectory_WithIncludePattern_OnlyReturnsFilesMatchingPattern(test_root):
    img_dir = test_root / "test-images/number-files"
    photoset = PhotosetLoader().load_directory(img_dir, include_pattern="00")
    images = [img.name for img in photoset]
    assert len(photoset) == 3
    assert "1000.jpg" in images
    assert "2200.jpg" in images
    assert "55000.png" in images


def test_PhotosetLoader_LoadDirectory_WithExcludePattern_ExcludesImagesMatchingPattern(test_root):
    img_dir = test_root / "test-images/number-files"
    photoset = PhotosetLoader().load_directory(img_dir, exclude_pattern="000")
    assert len(photoset) == 3


def test_PhotosetLoader_LoadDirectory_WithIncludePattern_AddsIncludedBackToList(test_root):
    img_dir = test_root / "test-images/number-files"
    photoset = PhotosetLoader().load_directory(img_dir, exclude_pattern="png$", include_pattern=r"0{2}")
    assert len(photoset) == 2


def test_PhotosetLoader_LoadDirectory_RecursiveTrue_ReturnsAllFilesInTree(test_root):
    img_dir = test_root / "test-images"
    photoset = PhotosetLoader().load_directory(img_dir, recursive=True)
    assert len(photoset) == 15


def test_PhotosetLoader_LoadDirectory_RecursiveFalse_OnlySearchesTopLevel(test_root):
    img_dir = test_root / "test-images"
    photoset = PhotosetLoader().load_directory(img_dir, recursive=False)
    assert len(photoset) == 0
