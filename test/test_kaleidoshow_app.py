import pytest

from kaleidoshow.carousel import CarouselSpecBuilder


def test_Kaleidoshow_AddCarousel_FolderWithImages_AddsCarouselToSavedListOnly(test_root, qapp_cls):
    img_path = test_root / "test-images/carousel"
    builder = CarouselSpecBuilder()
    spec = builder.build_from_paths(img_path)

    app = qapp_cls
    app.add_carousel(spec)
    assert len(app.saved_carousels) == 1
    assert len(app.open_carousels) == 0


def test_Kaleidoshow_AddCarousel_MissingFolder_StillAddsToSavedList(test_root, qapp_cls):
    img_path = test_root / "path/to/nowhere"
    builder = CarouselSpecBuilder()
    spec = builder.build_from_paths(img_path)

    app = qapp_cls
    app.add_carousel(spec)
    assert len(app.saved_carousels) == 1


def test_Kaleidoshow_OpenCarousel_AddsCarouselToOpenListOnly(test_root, qapp_cls):
    img_path = test_root / "test-images/carousel"
    builder = CarouselSpecBuilder()
    spec = builder.build_from_paths(img_path)

    app = qapp_cls
    app.open_carousel(spec)
    assert len(app.saved_carousels) == 0
    assert len(app.open_carousels) == 1


def test_Kaleidoshow_OpenCarousel_OpensCarouselWithCorrectValues(test_root, qapp_cls):
    data = {
        "name": "carousel-file",
        "paths": test_root / "test-images",
        "interval": 1,
        "mode": "shuffled",
        "recursive": True,
        "start_on_load": True,
        "show_tooltips": False,
    }
    builder = CarouselSpecBuilder()
    spec = builder.build_from_dict(data)

    app = qapp_cls
    app.open_carousel(spec)
    carousel = app.open_carousels[0]

    assert carousel.interval == 1
    assert carousel.running is True
    assert carousel.windows[0].photoset.mode == "shuffled"


def test_Kaleidoshow_OpenCarousel_EmptyFolder_DoesNothing(test_root, qapp_cls):
    data = {
        "name": "carousel-file",
        "paths": test_root / "test-images/empty-folder",
        "interval": 1,
        "mode": "shuffled",
        "recursive": False,
        "start_on_load": True,
        "show_tooltips": False,
    }
    builder = CarouselSpecBuilder()
    spec = builder.build_from_dict(data)

    app = qapp_cls
    app.open_carousel(spec)
    assert len(app.saved_carousels) == 0
    assert len(app.open_carousels) == 0


def test_Kaleidoshow_CarouselClosed_RemovesCarouselFromOpenList(test_root, qapp_cls):
    img_path = test_root / "test-images/carousel"
    builder = CarouselSpecBuilder()
    spec = builder.build_from_paths(img_path)

    app = qapp_cls
    app.open_carousel(spec)
    assert len(app.open_carousels) == 1
    was_closed = app.open_carousels[0].close()
    assert was_closed
    assert len(app.open_carousels) == 0
