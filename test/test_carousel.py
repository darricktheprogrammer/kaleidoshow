from pathlib import Path

import pytest
from box import BoxKeyError
from schema import SchemaError


from kaleidoshow.carousel import generate_carousel_name, CarouselSpecBuilder
from kaleidoshow.config import KaleidoshowConfig
from kaleidoshow.photoset import Photoset


def test_GenerateCarouselName_GivenPathlibPath_FormatsNameFromAllDirectoryNames():
    dirs = [
        Path("path/to/folder/with/images/imagedir-1"),
        Path("path/to/folder/with/images/imagedir-2"),
    ]
    carousel_name = generate_carousel_name(dirs)
    assert carousel_name == "imagedir-1 | imagedir-2"


def test_GenerateCarouselName_GivenPhotoset_FormatsNameFromAllSets():
    photosets = [
        Photoset("imagedir-1", "/no/path", []),
        Photoset("imagedir-2", "/no/path", []),
    ]
    carousel_name = generate_carousel_name(photosets)
    assert carousel_name == "imagedir-1 | imagedir-2"


def test_GenerateCarouselName_GivenIncompatibleObject_ThrowsAttributeError():
    photosets = [
        1,
        2,
    ]
    with pytest.raises(AttributeError):
        generate_carousel_name(photosets)


def test_GenerateCarouselName_GivenSingleStringPath_ThrowsAttributeError():
    with pytest.raises(AttributeError):
        generate_carousel_name(["/Path/to/single-image-dir"])


def test_GenerateCarouselName_GivenMultipleStringPaths_ThrowsAttributeError():
    dirs = [
        "path/to/folder/with/images/imagedir-1",
        "path/to/folder/with/images/imagedir-2",
    ]
    with pytest.raises(AttributeError):
        generate_carousel_name(dirs)


def test_CarouselSpecBuilder_SpecFromDict_FullDict_ReturnsCarouselSpec():
    data = {
        "name": "carousel-file",
        "paths": ["/path/to/one/image/folder"],
        "interval": 1,
        "mode": "shuffled",
        "start_on_load": True,
        "show_tooltips": False,
    }
    spec = CarouselSpecBuilder().build_from_dict(data)
    assert spec.name == "carousel-file"
    assert str(spec.paths[0].path) == "/path/to/one/image/folder"
    assert spec.interval == 1
    assert spec.mode == "shuffled"
    assert spec.start_on_load is True
    assert spec.show_tooltips is False


def test_CarouselSpecBuilder_SpecFromDict_PartialDict_ReturnsPartialCarouselSpec():
    data = {
        "name": "carousel-file",
        "paths": ["/path/to/one/image/folder"],
    }
    spec = CarouselSpecBuilder().build_from_dict(data)
    assert spec.name == "carousel-file"
    assert str(spec.paths[0].path) == "/path/to/one/image/folder"
    with pytest.raises(BoxKeyError):
        spec.interval


def test_CarouselSpecBuilder_SpecFromDict_PartialDictSingleStringPath_ReturnsCarouselSpec():
    data = {
        "name": "carousel-file",
        "paths": "/path/to/one/image/folder",
    }
    spec = CarouselSpecBuilder().build_from_dict(data)
    assert str(spec.paths[0].path) == "/path/to/one/image/folder"


def test_CarouselSpecBuilder_SpecFromPaths_SingleStringPath_ReturnsCarouselSpec():
    spec = CarouselSpecBuilder().build_from_paths("/path/to/one/image/folder")
    assert spec.name == "folder"


def test_CarouselSpecBuilder_SpecFromPaths_SinglePathlibPath_ReturnsCarouselSpec():
    spec = CarouselSpecBuilder().build_from_paths(Path("/path/to/one/image/folder"))
    assert spec.name == "folder"


def test_CarouselSpecBuilder_SpecFromPaths_MultiStringPath_ReturnsCarouselSpec():
    paths = [
        "path/to/folder/with/images/imagedir-1",
        "path/to/folder/with/images/imagedir-2",
    ]
    spec = CarouselSpecBuilder().build_from_paths(paths)
    assert spec.name == "imagedir-1 | imagedir-2"


def test_CarouselSpecBuilder_SpecFromFile_CarouselFile_ReturnsAllDefinedInFile(test_root):
    f = test_root / "test-config-files/carousel-file.crsl"
    spec = CarouselSpecBuilder().build_from_carousel_file(f)
    assert spec.name == "carousel-file"
    assert str(spec.paths[0].path) == "/path/to/one/image/folder"
    assert spec.paths[0].position is None
    assert spec.interval == 1
    assert spec.mode == "shuffled"
    assert spec.start_on_load is True
    assert spec.show_tooltips is False


def test_CarouselSpecBuilder_SpecFromFile_PathWithPosition_DefinesPosition(test_root):
    f = test_root / "test-config-files/carousel-file-with-position.crsl"
    spec = CarouselSpecBuilder().build_from_carousel_file(f)
    assert spec.name == "carousel-file-with-position"
    assert str(spec.paths[0].path) == "/path/to/one/image/folder"
    assert spec.paths[0].position == [1428, 733, 983, 707]
    assert spec.interval == 1
    assert spec.mode == "shuffled"
    assert spec.start_on_load is True
    assert spec.show_tooltips is False


def test_CarouselSpecBuilder_SpecFromDict_IsValidatingSchema(test_root):
    data = {
        "name": "carousel-file",
        "paths": ["/path/to/one/image/folder"],
        "interval": 0,
    }
    with pytest.raises(SchemaError):
        CarouselSpecBuilder().build_from_dict(data)


def test_CarouselSpec_UpdateWithDefaults_OnlyOverwritesMissingValues(test_root):
    defaults = KaleidoshowConfig.with_defaults()
    data = {
        "name": "carousel-file",
        "paths": ["/path/to/one/image/folder"],
        "interval": 1,
    }
    spec = CarouselSpecBuilder().build_from_dict(data)
    spec = spec.updated_with_defaults(defaults.carousel_settings)

    assert spec.name == "carousel-file"
    assert str(spec.paths[0].path) == "/path/to/one/image/folder"
    assert spec.interval == 1
    assert spec.mode == "ordered"

    # Ensure non-carousel related settings don't get added to the spec.
    with pytest.raises(BoxKeyError):
        spec.carousels
