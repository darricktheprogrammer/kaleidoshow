from pathlib import Path
from tempfile import TemporaryDirectory

import pytest
from schema import SchemaError, SchemaWrongKeyError

from kaleidoshow.config import KaleidoshowConfig
from kaleidoshow.schemas import (
    DEFAULTS_SCHEMA,
    USER_SCHEMA,
    CAROUSEL_SCHEMA,
)


def test_KaleidoshowConfig_NewInstance_StartsAsEmptyBox():
    config = KaleidoshowConfig()
    # Starts empty
    with pytest.raises(KeyError):
        config.interval

    # Key access through Box syntax
    config.interval = 30
    assert config.interval == 30


###############################################################################
#
# Schema Validations
#
###############################################################################

def test_KaleidoshowConfig_NewInstance_DefaultsPassSchemaValidation():
    config = KaleidoshowConfig.with_defaults()
    DEFAULTS_SCHEMA.validate(config)
    config.carousel_settings.interval = 1.5
    DEFAULTS_SCHEMA.validate(config)


def test_KaleidoshowConfig_LoadFromFile_AllowsMissingKeys(test_root):
    config_file = test_root / "test-config-files/empty-config.yml"
    config = KaleidoshowConfig.from_yaml(filename=config_file)
    USER_SCHEMA.validate(config)


def test_KaleidoshowConfig_BasicUserConfig_PassesSchemaValidation(test_root):
    config_file = test_root / "test-config-files/basic-config.yml"
    config = KaleidoshowConfig.from_yaml(filename=config_file)
    USER_SCHEMA.validate(config)


def test_KaleidoshowConfig_UnnestedCarouselSettings_FailsSchemaValidation(test_root):
    config_file = test_root / "test-config-files/invalid-config.yml"
    config = KaleidoshowConfig.from_yaml(filename=config_file)
    with pytest.raises(SchemaWrongKeyError):
        USER_SCHEMA.validate(config)


def test_KaleidoshowConfig_CarouselDict_PassesSchemaValidation(test_root):
    config_file = test_root / "test-config-files/named-carousel-config.yml"
    config = KaleidoshowConfig.from_yaml(filename=config_file)
    USER_SCHEMA.validate(config)


def test_KaleidoshowConfig_CarouselFile_PassesSchemaValidation(test_root):
    config_file = test_root / "test-config-files/carousel-file.crsl"
    config = KaleidoshowConfig.from_yaml(filename=config_file)
    CAROUSEL_SCHEMA.validate(config)


def test_KaleidoshowConfig_CarouselSpecificFilters_PassesSchemaValidation(test_root):
    config_file = test_root / "test-config-files/carousel-file.crsl"
    config = KaleidoshowConfig.from_yaml(filename=config_file)
    CAROUSEL_SCHEMA.validate(config)


def test_KaleidoshowConfig_InvalidMode_FailsSchemaValidation(test_root):
    config_file = test_root / "test-config-files/basic-config.yml"
    config = KaleidoshowConfig.from_yaml(filename=config_file)
    config.carousel_settings.mode = "merry-go-round"
    with pytest.raises(SchemaError):
        USER_SCHEMA.validate(config)


def test_KaleidoshowConfig_InvalidInterval_FailsSchemaValidation(test_root):
    config_file = test_root / "test-config-files/basic-config.yml"
    config = KaleidoshowConfig.from_yaml(filename=config_file)
    config.carousel_settings.interval = "three"
    with pytest.raises(SchemaError):
        USER_SCHEMA.validate(config)
    config.carousel_settings.interval = 0
    with pytest.raises(SchemaError):
        USER_SCHEMA.validate(config)


def test_KaleidoshowConfig_InvalidIntervalList_FailsSchemaValidation(test_root):
    config_file = test_root / "test-config-files/basic-config.yml"
    config = KaleidoshowConfig.from_yaml(filename=config_file)
    config.application_settings.intervals = [1, 2.5, "three"]
    with pytest.raises(SchemaError):
        USER_SCHEMA.validate(config)
    config.application_settings.intervals = [0, 5, 10]
    with pytest.raises(SchemaError):
        USER_SCHEMA.validate(config)


def test_KaleidoshowConfig_ExtraArguments_FailsSchemaValidation(test_root):
    config_file = test_root / "test-config-files/basic-config.yml"
    config = KaleidoshowConfig.from_yaml(filename=config_file)
    config.extra = "Surprise!"
    with pytest.raises(SchemaWrongKeyError):
        USER_SCHEMA.validate(config)


###############################################################################
#
# Value tests
#
###############################################################################

def test_KaleidoshowConfig_NewInstanceWithDefaults_LoadsDefaults():
    config = KaleidoshowConfig.with_defaults()
    assert config.application_settings.intervals == [1, 5, 10, 15, 20, 30, 45, 60]
    assert config.carousel_settings.interval == 30
    assert config.carousel_settings.mode == "ordered"
    assert config.carousel_settings.recursive is False
    assert config.carousel_settings.start_on_load is False
    assert config.carousel_settings.show_tooltips is True
    assert config.carousels == {}


def test_KaleidoshowConfig_BasicUserConfig_OverwritesDefaults(test_root):
    config_file = test_root / "test-config-files/basic-config.yml"
    config = KaleidoshowConfig.with_defaults()
    config.update_from_file(config_file)
    assert config.application_settings.intervals == [1, 3, 5, 7, 9, 15.5]
    assert config.carousel_settings.interval == 15
    assert config.carousel_settings.mode == "shuffled"
    assert config.carousel_settings.recursive is True
    assert config.carousel_settings.start_on_load is True
    assert config.carousel_settings.show_tooltips is False

    assert len(config.carousels) == 2
    single_image_dir = config.carousels.single_image_dir
    assert single_image_dir.name == "single-image-dir"
    assert len(single_image_dir.paths) == 1
    assert single_image_dir.paths[0].path == Path("/path/to/folder/with/single-image-dir")
    assert single_image_dir.paths[0].position is None

    multi_dir_carousel = config.carousels["imagedir-1 | imagedir-2"]
    assert multi_dir_carousel.name == "imagedir-1 | imagedir-2"
    assert len(multi_dir_carousel.paths) == 2
    assert multi_dir_carousel.paths[0].path == Path("path/to/folder/with/images/imagedir-1")
    assert multi_dir_carousel.paths[0].position is None
    assert multi_dir_carousel.paths[1].path == Path("path/to/folder/with/images/imagedir-2")
    assert multi_dir_carousel.paths[0].position is None


def test_KaleidoshowConfig_UserConfigMissingCarouselKey_DoesNotError(test_root):
    config_file = test_root / "test-config-files/config-without-carousels.yml"
    config = KaleidoshowConfig.with_defaults()
    config.update_from_file(config_file)


def test_KaleidoshowConfig_UserConfigWithDictCarousel_LoadsCarouselDefinitions(test_root):
    config_file = test_root / "test-config-files/named-carousel-config.yml"
    config = KaleidoshowConfig.with_defaults()
    config.update_from_file(config_file)
    carousels = config.carousels
    assert len(carousels.keys()) == 2
    assert carousels.custom_carousel.name == "custom-carousel"
    assert carousels.custom_carousel.interval == 5


@pytest.mark.skip(reason="Cannot create repeatable environment for load_known_configs")
def test_KaleidoshowConfig_LoadKnownConfigs_DoesNotErrorOnMissingConfigs(test_root):
    # load_known_configs looks in a series of likely locations for config
    # files. It is expected that at least one of those files doesn't exist, so
    # it's important to test that Kaleidoshow doesn't crash trying to load
    # them. However, if Kaleidoshow is installed on the running computer,
    # uncontrolled configs will dirty the environment.
    #
    # This test acts as a placeholder for any tests needing to be done
    # involving known config paths. When modifying, comment out the skip mark,
    # ensure configs are in a known state and create any tests needed for
    # verification. Revert this to its original form before committing.
    config = KaleidoshowConfig()
    config.load_known_configs()


def test_KaleidoshowConfig_LoadCarouselFile_ReturnsCarouselDetails(test_root):
    config_file = test_root / "test-config-files/carousel-file.crsl"
    carousel = KaleidoshowConfig.from_carousel_file(config_file)
    assert type(carousel) == KaleidoshowConfig
    assert carousel.name == "carousel-file"
    assert carousel.paths == ["/path/to/one/image/folder"]
    assert carousel.interval == 1
    assert carousel.mode == "shuffled"
    assert carousel.recursive is True
    assert carousel.start_on_load is True
    assert carousel.show_tooltips is False


def test_KaleidoshowConfig_Save_SavesConfigChanges():
    config = KaleidoshowConfig.with_defaults()
    config.application_settings.intervals = [1, 10, 30]
    config.carousel_settings.interval = 1
    config.carousel_settings.mode = "shuffled"
    config.carousel_settings.recursive = True
    config.carousel_settings.start_on_load = True
    config.carousel_settings.show_tooltips = False

    with TemporaryDirectory() as tempdir:
        config_path = Path(tempdir) / "prefs.yml"
        config.save(filepath=config_path)

        new_config = KaleidoshowConfig.from_yaml(filename=config_path)
        assert new_config.application_settings.intervals == [1, 10, 30]
        assert new_config.carousel_settings.interval == 1
        assert new_config.carousel_settings.mode == "shuffled"
        assert new_config.carousel_settings.recursive is True
        assert new_config.carousel_settings.start_on_load is True
        assert new_config.carousel_settings.show_tooltips is False


def test_KaleidoshowConfig_Save_DoesNotSaveCarousels():
    # carousels are not saved in the main app config. Only within
    # custom-created config files.
    config = KaleidoshowConfig.with_defaults()
    config.carousels = {
        "custom-carousel": {
            "name": "custom-carousel",
            "paths": "/path/to/image/folder",
            "interval": 5
            }
        }

    with TemporaryDirectory() as tempdir:
        config_path = Path(tempdir) / "prefs.yml"
        config.save(filepath=config_path)

        new_config = KaleidoshowConfig.from_yaml(filename=config_path)
        assert new_config.carousels == {}
        # ensure carousels weren't removed from the original
        assert config.carousels != {}


def test_KaleidoshowConfig_Save_CreatesPathIfItDoesntExist():
    config = KaleidoshowConfig.with_defaults()
    with TemporaryDirectory() as tempdir:
        config_path = Path(tempdir) / "subfolder/prefs.yml"
        config.save(filepath=config_path)
