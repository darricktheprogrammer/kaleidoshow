from __future__ import annotations

import re
import logging
import random
from pathlib import Path
from functools import cache

from PySide6.QtGui import QImageReader
import natsort as ns


log = logging.getLogger(__name__)


SUPPORTED_FORMATS = [
    f".{f.data().decode()}" for f in QImageReader.supportedImageFormats()
]


class Photoset:
    """A List of files representing the slideshow."""

    def __init__(self, name: str, root_directory: str, filelist: list[Path]):
        self.name = name
        self.root_directory = root_directory
        self.mode = "ordered"
        self._filelist = filelist
        self._index = 0

    def __len__(self) -> int:
        return len(self._filelist)

    def __eq__(self, other: Photoset) -> bool:  # type: ignore
        return self._filelist == other._filelist

    def __iter__(self):
        for f in self._filelist:
            yield f

    def next(self) -> Path:
        if self._index == len(self) - 1:
            self._index = 0
        else:
            self._index += 1
        return self.current_file

    def previous(self) -> Path:
        if self._index == 0:
            self._index = len(self._filelist) - 1
        else:
            self._index -= 1
        return self.current_file

    def convert_to_shuffled_photoset(self) -> ShuffledPhotoset:
        return ShuffledPhotoset(self.name, self.root_directory, self._filelist)

    @property
    def current_file(self) -> Path:
        return self._filelist[self._index]


class ShuffledPhotoset(Photoset):
    def __init__(self, name: str, root_directory: str, filelist: list[Path]):
        super().__init__(name, root_directory, filelist)
        self.mode = "shuffled"
        self._original_list = filelist
        shuffled = filelist.copy()
        random.shuffle(shuffled)
        self._filelist = shuffled

    def reshuffle(self) -> None:
        random.shuffle(self._filelist)

    def convert_to_ordered_photoset(self) -> Photoset:
        ordered_file_index = self._original_list.index(self.current_file)
        ordered_photoset = Photoset(self.name, self.root_directory, self._original_list)
        ordered_photoset._index = ordered_file_index
        return ordered_photoset


class PhotosetLoader:
    def load_directory(
        self,
        fp: Path,
        recursive: bool = False,
        shuffled: bool = False,
        exclude_pattern: str = "",
        include_pattern: str = "",
    ) -> Photoset:
        PhotosetClass = ShuffledPhotoset if shuffled else Photoset
        images = self._search_for_images(fp, recursive=recursive)
        if exclude_pattern:
            images = self._exclude(exclude_pattern, images)
        if include_pattern:
            images = self._include(include_pattern, images)
        return PhotosetClass(fp.name, str(fp), ns.natsorted(images, alg=ns.IGNORECASE))

    @cache
    def _search_for_images(self, fp: Path, recursive: bool = False) -> list:
        all_files = []
        for pth in fp.iterdir():
            if pth.suffix.lower() in SUPPORTED_FORMATS:
                all_files.append(pth)
            elif recursive and pth.is_dir():
                all_files.extend(self._search_for_images(pth, recursive=recursive))
        return all_files

    def _exclude(self, pattern: str, image_list: list[Path]) -> list[Path]:
        re_pattern = re.compile(pattern, re.I)
        return [pth for pth in image_list if not re_pattern.search(str(pth))]

    def _include(self, pattern: str, image_list: list[Path]) -> list[Path]:
        re_pattern = re.compile(pattern, re.I)
        return [pth for pth in image_list if re_pattern.search(str(pth))]
