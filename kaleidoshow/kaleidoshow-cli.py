"""
Command line entry point into Kaleidoshow.

Kaleidoshow is an image slideshow viewer heavily inspired by Photostickies from
DEVON Technologies. This cli entry point is not meant for public use. It is a
development environment to be packaged up for release as a .app or .exe file.
"""
import sys
import logging
from argparse import ArgumentParser
from argparse import Namespace
from typing import Any, Iterable

from kaleidoshow import applogging
from kaleidoshow.app import Kaleidoshow
from kaleidoshow.carousel import CarouselSpecBuilder


log = logging.getLogger(__package__)


def set_logging(args: Namespace) -> None:
    if args.verbose:
        applogging.setLevel("DEBUG")
    elif args.quiet:
        applogging.setLevel("WARN")
    applogging.init_logger()


def main(
    carousels: Iterable[str],
    extra_configs: Iterable[str],
    ignore_known_configs: bool,
    recursive: bool = False,
) -> None:
    # I don't fully understand the internal mechanisms in QApplication for
    # reading command line arguments, but sys.argv must be passed to prevent
    # errors on load. Passing nothing or an empty list causes a QFileEvent to
    # trigger for each argument. Explicitly passing sys.argv prevents it.
    kwargs: dict[str, Any] = {}
    kwargs.update(
        extra_configs=extra_configs, ignore_known_configs=ignore_known_configs
    )

    app = Kaleidoshow(sys.argv, **kwargs)
    builder = CarouselSpecBuilder()
    if recursive:
        app.config.carousel_settings.recursive = True
    for carousel in carousels:
        app.open_carousel(builder.build_from_paths(carousel))
    app.exec()


def cli_main() -> None:
    """
    CLI-specific entry point.

    Acts as an intermediary to `main()` allowing the file to be called from the
    command line, or called directly from other Python code.
    """
    parser = ArgumentParser(description=__doc__)
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        default=False,
        help="Increase level of output.",
    )
    parser.add_argument(
        "-q",
        "--quiet",
        action="store_true",
        default=False,
        help="Decrease level of output.",
    )
    parser.add_argument(
        "-c",
        "--carousel",
        nargs="*",
        action="append",
        default=list(),
        help=(
            "A List of directories to load into a carousel at runtime."
            " You may specify --carousel more than once and each subsequent"
            " list of directories will be loaded into a separate carousel."
        ),
    ),
    parser.add_argument(
        "-r",
        "--recursive",
        action="store_true",
        default=False,
        help=(
            "Tell Kaleidoshow to load images recursively during this session."
            " This overwrites the recursive property globally for duration of"
            " the session. Any directories opened during the session will be"
            " searched recursively unless it is a carousel defined in a config"
            " file which has its own recursive property defined. This setting"
            " is not saved after quitting."
        ),
    )
    parser.add_argument(
        "--extra-configs",
        nargs="*",
        action="extend",
        default=None,
        help="Additional config files to be read on startup.",
    )
    parser.add_argument(
        "--ignore-known-configs",
        action="store_true",
        default=False,
        help=(
            "Do not search for config files in standard user locations"
            " such as $XDG_CONFIG_HOME and %APPDATA%. This option is used"
            " mostly for development, to ensure a clean environment."
        ),
    )
    args = parser.parse_args()
    set_logging(args)
    main(
        args.carousel,
        args.extra_configs,
        args.ignore_known_configs,
        recursive=args.recursive,
    )


if __name__ == "__main__":
    cli_main()
