from logging.config import dictConfig
from platformdirs import user_log_path

LOG_FILE = user_log_path("Kaleidoshow") / "kaleidoshow.log"

LOGGING_CONFIG = {
    "version": 1,
    "loggers": {
        "kaleidoshow": {
            "level": "INFO",
            "propagate": False,
            "handlers": ["rotating_logger"],
        },
    },
    "handlers": {
        "rotating_logger": {
            "formatter": "info",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": LOG_FILE,
            "maxBytes": 512_000,
            "backupCount": 2,
        },
    },
    "formatters": {
        "info": {
            "format": "%(asctime)s [%(levelname)s] %(message)s",
            "datefmt": "%Y-%m-%d %H:%M:%S",
        },
    },
}


def setLevel(level):
    LOGGING_CONFIG["loggers"]["kaleidoshow"]["level"] = level


def init_logger():
    LOG_FILE.parent.mkdir(parents=True, exist_ok=True)
    dictConfig(LOGGING_CONFIG)
