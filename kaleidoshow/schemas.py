"""Schema definitions for validating config files."""
from pathlib import Path

from schema import Schema, And, Or, Optional  # type: ignore


_VALID_MODE = Or("ordered", "shuffled", error="mode must be one of: ordered|shuffled")
_VALID_INTERVAL = And(
    Or(int, float), lambda n: n > 0, error="Interval must be a number > 0"
)
_qt_rect = And([int], lambda item: len(item) == 4)
_VALID_PATH_DICT = And(
    {
        "path": Or(str, Path),
        "position": Or(_qt_rect, None),
    },
    error="`path` must be a string and `position` must be a list of 4 integers or null",
)
_VALID_PATH_LIST = [Or(str, [str], Path, _VALID_PATH_DICT)]

CAROUSEL_SCHEMA = Schema(
    {
        "name": str,
        "paths": _VALID_PATH_LIST,
        Optional("interval"): _VALID_INTERVAL,
        Optional("mode"): _VALID_MODE,
        Optional("recursive"): bool,
        Optional("start_on_load"): bool,
        Optional("show_tooltips"): bool,
        Optional("show_tooltips"): bool,
        Optional("exclude_pattern"): str,
        Optional("include_pattern"): str,
    }
)

DEFAULTS_SCHEMA = Schema(
    {
        "application_settings": {
            "intervals": [_VALID_INTERVAL],
        },
        "carousel_settings": {
            "interval": _VALID_INTERVAL,
            "mode": _VALID_MODE,
            "recursive": bool,
            "start_on_load": bool,
            "show_tooltips": bool,
            "exclude_pattern": str,
            "include_pattern": str,
        },
        "carousels": dict,
    }
)


multiple_carousels = [CAROUSEL_SCHEMA]

USER_SCHEMA = Schema(
    {
        Optional("application_settings"): {
            Optional("intervals"): [_VALID_INTERVAL],
        },
        Optional("carousel_settings"): {
            Optional("interval"): _VALID_INTERVAL,
            Optional("mode"): _VALID_MODE,
            Optional("recursive"): bool,
            Optional("start_on_load"): bool,
            Optional("show_tooltips"): bool,
            Optional("exclude_pattern"): str,
            Optional("include_pattern"): str,
        },
        Optional("carousels"): Or(_VALID_PATH_LIST, multiple_carousels),
    }
)
