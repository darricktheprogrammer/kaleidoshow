"""Handle loading and saving app configurtation."""
from __future__ import annotations

import logging
from copy import deepcopy
from pathlib import Path
from os import PathLike  # Used by Box, but not part of the typing module
from typing import Optional

from box import Box, BoxError
from platformdirs import user_data_path
from xdg import xdg_config_home, xdg_config_dirs

from . import environment
from .carousel import CarouselSpecBuilder

log = logging.getLogger(__name__)

DEFAULT_CONFIG = environment.APP_ROOT / "defaults.yml"

# The location where Kaleidoshow will save settings when modified in the UI.
# This location is platform specific and generated at runtime.
APPDIR = Path(user_data_path(appname="Kaleidoshow"))

SAVED_SETTINGS_FILE = APPDIR / "config.yml"

CONFIG_DIRS = [
    *[pth / "Kaleidoshow" for pth in xdg_config_dirs()],
    xdg_config_home() / "Kaleidoshow",
    APPDIR,
]

CONFIG_PATHS = [pth / "config.yml" for pth in CONFIG_DIRS]
"""
A list of known config file locations in ascending priority order.

Kaleidoshow searches in the following order:
1. $XDG_CONFIG_DIRS/Kaleidoshow/config.yml
2. $XDG_CONFIG_HOME/Kaleidoshow/config.yml
3. Platform specific App directory (~/Library/Application Support on Mac,
   %APPDATA% on Windows). Locations are provided by
   [platformdirs](https://github.com/platformdirs) See their documentation for
   more information.

It was decided to put the App directory last, as that will hold settings that
were changed by the user when the app is running. These settings are more
likely to be desired, as they are changed while using the app, and do not
require the user to leave the app to edit a text file.
"""


class KaleidoshowConfig(Box):
    """
    Holds the settings for the running application.

    KaleidoshowConfig subclasses [Box](https://github.com/cdgriffith/Box). This
    is mostly convenience to inherit dot-notation access, but comes with a
    large amount of other helpful features as a bonus. See Box's documentation
    for more details.
    """

    @classmethod
    def with_defaults(cls) -> KaleidoshowConfig:
        """
        Create a new instance with the settings defined in defaults.yaml.

        This should be the primary method for creating a new KaleidoshowConfig
        instance.

        Creating a new instance using KaleidoshowConfig() directly or
        KaleidoshowConfig.from_yaml() can still be done, but you should have a
        specific reason for doing it. Those methods are meant for managing the
        values during testing.

        Originally, KaleidoshowConfig loaded default.yml during initialization.
        Because loading the defaults during initialization is magic behavior,
        it caused issues during testing as every time a new instance was
        created by calling self.from_yaml, the defaults were read again and
        would override modified settings.

        Returns:
            KaleidoshowConfig
        """
        log.info(f"Loading default configuration: {DEFAULT_CONFIG}")
        return cls.from_yaml(filename=DEFAULT_CONFIG)  # type: ignore

    @classmethod
    def from_carousel_file(cls, pth: PathLike) -> KaleidoshowConfig:
        """
        Create a new KaleidoshowConfig object from a Carousel definition file.

        Carousel definition files (.crsl) are standalone files defining a
        carousel. They are made to be easy to load and append directly into the
        list of existing carousels that already exist within the config or
        create a new Carousel with no processing overhead.

        Args:
            pth (os.PathLike): Path to the file
        Returns:
            KaleidoshowConfig
        """
        log.info(f"Loading Carousel File: {pth}")
        return cls.from_yaml(filename=pth)  # type: ignore

    def __repr__(self):
        # Box hardcodes its name into the __repr__ instead of using type(self).
        # This can be confusing when print debugging, as you are expecting a
        # KaleidoshowConfig object, but think you have a Box object.
        #
        # Only replace the first instance, because there are actual Box objects
        # within the internal dictionary.
        repr_ = super().__repr__()
        return repr_.replace("Box(", "KaleidoshowConfig(", 1)

    def load_known_configs(self) -> None:
        """
        Load configuration from several likely locations.

        It is not expected that all of the known locations exist. And it is
        allowed for none of the to exist. See CONFIG_PATHS for more details
        """
        log.info("Loading configuration from known files...")
        for conf in CONFIG_PATHS:
            self.update_from_file(conf)
        self.load_carousel_files()

    def load_carousel_files(self):
        log.info("Loading saved carousel files...")
        for f in self.search_known_dirs_for_carousel_files():
            carousel = self.from_carousel_file(f)
            self.carousels.merge_update({carousel.name: carousel})

    def update_from_file(self, pth: PathLike) -> None:
        """
        Update the configuration in place with values from a file.

        While load_known_configs will do most of the heavy lifting for
        configuration, this allows additional configuration when necessary. It
        is especially useful when testing or passing --extra-config from the
        command line.

        Args:
            pth (os.PathLike): Path to the file
        """
        # carousel definitions can either be a dict of values, or a simple list
        # of paths. If given a list of paths, it will need to be converted into
        # a dict.
        try:
            conf = self.from_yaml(filename=pth)
        except BoxError:
            log.info(f"Failed to load configuration file: {pth}. File missing.")
            return

        # It is not mandatory for a user to define an empty `carousel` key
        if carousels := conf.get("carousels", False):
            carousel_dict = {}
            carousels = conf.carousels
            builder = CarouselSpecBuilder()
            for c in carousels:
                if isinstance(c, (list, str)):
                    spec = builder.build_from_paths(c)
                    carousel_dict[spec.name] = spec
                else:
                    spec = builder.build_from_dict(c)
                    carousel_dict[c.name] = c

            log.info(f"Loaded configuration from: {pth}")
            log.debug(f"Saved Carousels loaded: {carousel_dict}")
            conf.carousels = carousel_dict
        self.merge_update(conf)

    def save(self, filepath: Optional[Path] = None) -> None:
        if filepath is None:
            filepath = SAVED_SETTINGS_FILE
        filepath.parent.mkdir(exist_ok=True, parents=True)

        # Carousels are not saved in the main app config. Create a copy of the
        # config so we can remove carousels without messing up the active
        # configuration.
        savable_config = deepcopy(self)
        if not filepath.suffix == ".crsl":
            savable_config.carousels = dict()
        savable_config.to_yaml(filename=filepath)

    def search_known_dirs_for_carousel_files(self) -> list[Path]:
        files: list[Path] = []
        for pth in CONFIG_DIRS:
            files.extend(pth.glob("*.crsl"))
        return files
