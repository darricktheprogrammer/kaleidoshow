"""Controllers for managing and displaying carousels."""
from __future__ import annotations

import logging
from pathlib import Path
from typing import Sequence, Union, Optional
from os import PathLike
from functools import partial

from box import Box
from PySide6.QtWidgets import QWidget, QLabel, QApplication
from PySide6.QtGui import QPixmap, QShortcut, QKeySequence
from PySide6.QtCore import Qt, QTimer, Signal

from . import environment
from .ui import ImageWindow
from .ui.menus import ContextMenu
from .photoset import Photoset, PhotosetLoader
from .schemas import CAROUSEL_SCHEMA

log = logging.getLogger(__name__)


DEFAULT_IMAGE = environment.APP_ROOT / "assets/default-image.png"


def generate_carousel_name(photosets: Sequence[Union[Path, Photoset]]) -> str:
    """
    Generate a name on-the-fly for an unnamed Carousel.

    This is only necessary when creating a new Carousel either:

    1. Through the UI, where a transient Carousel is being created and would
       add an extra step for the user to name it.
    2. From an unnamed Carousel definition in a config file. In that case, the
       Carousel is simply a list of paths and the user has not felt it
       important to name the Carousel.

    This happens to work with both Photosets and Paths because they both have a
    `name` property. This is a "good enough" solution at this time, but future
    updates could break compatibility with one type or the other if their API
    changes. Strings are not valid input.

    Args:
        photosets (Path, Photoset): A list representing top level folders.
            It is expected that each entry will be given its own window.
            Technically, only 2 types are supported, but any object with a
            `name` property will be accepted.
    Returns:
        str
    """
    return " | ".join([pth.name for pth in photosets])


class PhotosetEmptyError(Exception):
    pass


class DisplayWindow(ImageWindow):
    """
    Display Manager for a single Photoset.

    The DisplayWindow manages displaying images in the photoset and responding
    to user input from the GUI.

    Args:
        photoset (Photoset): The photoset that will be displayed in the window.
            The DisplayWindow does not handle any of the processing or
            traversing of the photoset, but it holds the only reference to it
            as the window is the only place images from the photoset can be
            displayed.
        carousel_name (str): The name of the Carousel the Window belongs to.
            This property is only used for display purposes.
    """

    user_next_request = Signal(QLabel, bool)
    user_previous_request = Signal(QLabel, bool)
    user_interval_update = Signal(float)
    carousel_start = Signal()
    carousel_stop = Signal()
    window_closed = Signal(QLabel)
    window_started = Signal()
    window_stopped = Signal()
    reload_photoset = Signal()

    def __init__(
        self,
        photoset: Photoset,
        show_tooltips: bool = True,
        carousel=None,
    ):
        super().__init__()
        self.carousel = carousel
        if len(photoset) == 0:
            raise PhotosetEmptyError(f"No images found in photoset: {photoset.name}")
        self.photoset = photoset
        self.show_tooltips = show_tooltips
        self.current_image = photoset.current_file
        self.running = True
        log.info(
            f"Initializing window for photoset '{self.photoset.name}' with image:"
            f" {self.current_image}"
        )
        self.setAttribute(Qt.WA_AlwaysShowToolTips)  # type: ignore
        self.setAttribute(Qt.WA_DeleteOnClose)  # type: ignore
        self.setAcceptDrops(True)
        if not self.current_image.exists():
            self.current_image = DEFAULT_IMAGE
            log.warn(
                f"Initial image missing for photoset '{self.photoset.name}'. Replaced"
                f" with {self.current_image}"
            )
        self.setPixmap(QPixmap(self.current_image))
        self._update_tooltip()
        self.carousel.carousel_name_changed.connect(self._update_tooltip)
        self.carousel.carousel_closed.connect(self.close)
        self._init_keyboard_shortcuts()

    def start(self) -> None:
        self.running = True
        self.window_started.emit()

    def stop(self) -> None:
        self.running = False
        self.window_stopped.emit()

    def swap(self, img: Path) -> None:
        if not img.exists():
            msg = f"Image update failed in photoset '{self.photoset.name}'."
            msg += f" New file does not exist or symlink is broken: {img}"
            log.warn(msg)
            return
        log.debug(
            f"Replacing displayed image in photoset '{self.photoset.name}':"
            f" {self.current_image}"
        )
        log.info(f"Displaying new image in photoset '{self.photoset.name}': {img}")

        self.setPixmap(QPixmap(img))
        self.current_image = img
        self._update_tooltip()

    def _update_tooltip(self) -> None:
        if not self.show_tooltips:
            return
        img = self.current_image
        folder_name = img.parent.name
        msg = "<p style='white-space:pre'>"
        msg += f"<b>[{self.carousel.name}]</b><br>"
        msg += f"{img.name}<br><br>"
        if self.photoset.name != folder_name:
            msg += f"Folder: {folder_name}<br>"
        msg += f"Photoset: {self.photoset.name}<br>"
        msg += f"Path: {str(img)}"
        self.setToolTip(msg)

    def _init_keyboard_shortcuts(self):
        next_img_keys = QShortcut(QKeySequence.MoveToEndOfLine, self)
        next_img_keys.activated.connect(
            partial(self.user_next_request.emit, self, True)
        )
        prev_img_keys = QShortcut(QKeySequence.MoveToStartOfLine, self)
        prev_img_keys.activated.connect(
            partial(self.user_previous_request.emit, self, True)
        )

    def contextMenuEvent(self, event):
        menu = ContextMenu(self)
        menu.exec(event.globalPos())

    def closeEvent(self, event):
        self.window_closed.emit(self)
        super().closeEvent(event)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.acceptProposedAction()
        else:
            event.ignore()

    def dropEvent(self, event):
        md = event.mimeData()
        if md.hasUrls():
            for url in md.urls():
                abs_url = url.toLocalFile()
                app = QApplication.instance()
                carousel = self.carousel
                msg = f"Drop event received on window '{self.photoset.name}'"
                msg += f" in carousel '{carousel.name}'."
                msg += f" Adding new window: {abs_url}"
                log.info(msg)

                photoset = PhotosetLoader().load_directory(
                    Path(abs_url),
                    recursive=app.config.carousel_settings.recursive,
                    shuffled=app.config.carousel_settings.mode == "shuffled",
                    exclude_pattern=app.config.carousel_settings.exclude_pattern,
                    include_pattern=app.config.carousel_settings.include_pattern,
                )

                window = DisplayWindow(
                    photoset,
                    show_tooltips=self.show_tooltips,
                    carousel=carousel,
                )
                self.carousel.add_window(window)
                self.carousel.name += f" | {photoset.name}"
                window.show()
                app.save_recent(abs_url)
            event.acceptProposedAction()


class Carousel(QWidget):
    """
    Main manager for sets of slideshows.

    A Carousel manages a linked set of Photosets, updating the displayed
    image from each photoset in a cycle. A single-photoset Carousel is simply a
    special case where the Carousel only contains one displayed image at a
    time.

    Args:
        name (str): The name of the Carousel
        photosets (list[Photoset]): One or more Photosets. Each Photoset will
        be displayed in its own window.
    Kwargs:
        interval (int|float): How long each image in the Carousel will be
            displayed. This means that for Carousels with multiple photosets,
            the Carousel will update each window at a rate of
            {interval/len(photosets)}

            For instance, a single-photoset Carousel with interval 20 will
            update the image every 20 seconds, while a Carousel with 2
            photosets and 20 interval will update the first image, then update
            the 2nd image 10 seconds later, finally updating the first image
            again in 10 seconds. So, each image is shown for 20 seconds, while
            each photoset is updated in regular intervals to the entire
            Carousel.
        show_tooltips (bool): Whether or not to show image details through a
            tooltip when the user hovers over the image.
    """

    carousel_closed = Signal(QWidget)
    carousel_name_changed = Signal()

    def __init__(
        self,
        name: str,
        interval: Optional[Union[int, float]] = None,
    ):
        super().__init__()
        self.name = name
        config = QApplication.instance().config.carousel_settings  # type: ignore
        if interval is None:
            interval = config.interval
        self.windows: list[DisplayWindow] = []

        self.setAttribute(Qt.WA_DeleteOnClose)  # type: ignore
        # Set to last item, so the first update will loop around to the first
        self._window_index = len(self.windows)
        self._timer = QTimer()
        self._timer.timeout.connect(self.display_next)  # type: ignore
        self.interval = interval
        self.update_interval(interval)
        self.running = False
        self._closing = False

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, new_name: str) -> None:
        self._name = new_name
        self.carousel_name_changed.emit()

    @property
    def _effective_interval(self):
        if len(self.windows) == 0:
            eff_int = self.interval * 1000
        else:
            eff_int = int(self.interval / len(self.windows) * 1000)
        log.debug(f"Effective interval evaluated to: {eff_int}")
        return eff_int

    def start(self) -> None:
        if not self.running:
            log.info(f"Starting Carousel: {self.name}...")
            self.running = True
            if not self._timer.isActive():
                # If there is still an active timer, let it run out first. This
                # way, if interval is set at 10, user stops the carousel, then
                # starts it again in 5 seconds, the carousel will wait another
                # 5 seconds for the timer before showing the next image. If the
                # timer has already expired (>10 seconds), immediately show the
                # next image and start the timer.
                self._display_next(self.windows[self._window_index])
                self._timer.start()

    def stop(self) -> None:
        if not self.running:
            log.info(f"Carousel '{self.name}' already stopped.")
            return
        log.info(f"Stopping Carousel: {self.name}...")
        self.running = False

    def stop_if_no_running_windows(self) -> None:
        """
        Stop the Carousel if all windows it manages are stopped.

        If none of the windows in the Carousel are running, there's no point in
        continuing to cycle through them to check. CPU cycles are cheap, but
        the additional checks will add unnecessary log entries.

        This is also good UX for single-window Carousels. The user can click
        `Stop` or `Stop Carousel`. In my experience, it is unintuitive to click
        `Stop Carousel` when you know there is only a single window.
        """
        any_window_running = any([w.running for w in self.windows])
        if not any_window_running and self.running:
            log.info(f"All windows in Carousel '{self.name}' have been stopped.")
            self.stop()

    def remove_window(self, window: DisplayWindow) -> None:
        log.info(f"Closing window '{window.photoset.name}' in Carousel '{self.name}'")
        self.windows.remove(window)
        if len(self.windows) == 0 and not self._closing:
            self.close()
        else:
            self.update_interval(self.interval)

    def display_next(self) -> None:
        window = self.next_window()
        self._display_next(window)

    def next_window(self) -> DisplayWindow:
        self._window_index += 1
        if self._window_index >= len(self.windows):
            self._window_index = 0
        return self.windows[self._window_index]

    def update_interval(self, interval: Union[int, float]) -> None:
        self.interval = interval
        self._timer.setInterval(self._effective_interval)
        log.info(f"Carousel '{self.name}' interval upated: {interval}")

    def add_window(self, window: DisplayWindow) -> None:
        self.windows.append(window)
        window.user_next_request.connect(self._display_next)
        window.user_previous_request.connect(self._display_previous)
        window.user_interval_update.connect(self.update_interval)
        window.carousel_start.connect(self.start)
        window.carousel_stop.connect(self.stop)
        window.window_closed.connect(self.remove_window)
        window.window_stopped.connect(self.stop_if_no_running_windows)
        window.window_started.connect(self.start)
        self.update_interval(self.interval)

    def _display_next(self, window: DisplayWindow, force=False) -> None:
        if not self.running and not force:
            self._timer.stop()
            return
        photoset = window.photoset
        if not window.running and not force:
            log.info(
                f"Next image requested for photoset '{photoset.name}', but window is"
                " paused."
            )
            return
        window.swap(photoset.next())

    def _display_previous(self, window: DisplayWindow, force=False) -> None:
        if not self.running and not force:
            self._timer.stop()
            return
        photoset = window.photoset
        if not window.running and not force:
            log.info(
                f"Previous image requested for photoset '{photoset.name}', but window"
                " is paused."
            )
            return
        window.swap(photoset.previous())

    def show(self):
        for w in self.windows:
            w.show()

    def close(self):
        self._closing = True
        self.carousel_closed.emit(self)
        return super().close()


class CarouselSpec(Box):
    def updated_with_defaults(self, defaults: dict) -> CarouselSpec:
        spec = defaults | self
        log.debug(f"Updating CarouselSpec: {self} => {defaults}")
        return CarouselSpec(spec)


class WindowPathSpec(Box):
    def __init__(self, path: Path, position: Optional[list[int]]):
        super().__init__()
        self.path = path
        self.position = position

    @classmethod
    def from_path(cls, pth: Union[str, Path]) -> WindowPathSpec:
        return cls(Path(pth), None)

    @classmethod
    def from_dict(cls, dict_: dict) -> WindowPathSpec:
        return cls(Path(dict_["path"]), dict_["position"])


class CarouselSpecBuilder:
    complex_path_type = Union[str, Path, dict, list[Union[str, Path, dict]]]

    def build_from_dict(self, d: dict) -> CarouselSpec:
        d_copy = d.copy()
        name, paths = d_copy.pop("name"), d_copy.pop("paths")
        paths = self._normalize_path_list(paths)
        spec = CarouselSpec(name=name, paths=paths, **d_copy)
        CAROUSEL_SCHEMA.validate(spec)
        return spec

    def build_from_paths(self, paths: complex_path_type) -> CarouselSpec:
        normalized_list = self._normalize_path_list(paths)
        carousel_def = self._basic_dict_from_path_list(normalized_list)
        return self.build_from_dict(carousel_def)

    def build_from_carousel_file(self, fp: PathLike) -> CarouselSpec:
        return self.build_from_dict(Box.from_yaml(filename=fp))

    def _normalize_path_list(self, ls: complex_path_type) -> list:
        if isinstance(ls, (str, Path, dict)):
            ls = [ls]

        flattened = []
        for item in ls:
            if isinstance(item, dict):
                flattened.append(WindowPathSpec.from_dict(item))
            elif isinstance(item, (str, Path)):
                flattened.append(WindowPathSpec.from_path(item))
            else:
                flattened.extend([WindowPathSpec.from_path(p) for p in item])
        return flattened

    def _basic_dict_from_path_list(self, ls: list[WindowPathSpec]) -> dict:
        c_name = generate_carousel_name([pathspec.path for pathspec in ls])
        return {"name": c_name, "paths": ls}
