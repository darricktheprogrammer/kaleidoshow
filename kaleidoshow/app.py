"""Main application."""
import logging
from typing import Optional
from os import PathLike

from PySide6.QtWidgets import QApplication, QFileDialog, QInputDialog
from PySide6.QtGui import QImageReader, QFileOpenEvent
from PySide6.QtCore import QSettings, QRect

from . import environment
from kaleidoshow.config import KaleidoshowConfig, APPDIR
from kaleidoshow.photoset import Photoset, PhotosetLoader
from kaleidoshow.carousel import (
    Carousel,
    DisplayWindow,
    CarouselSpecBuilder,
    CarouselSpec,
    WindowPathSpec,
)
from kaleidoshow.carousel import PhotosetEmptyError
from kaleidoshow.ui import PrefsWindow
from kaleidoshow.ui.menus import KaleidoshowMenuBar


log = logging.getLogger(__name__)


class NoWindowsToCreateError(Exception):
    pass


class Kaleidoshow(QApplication):
    def __init__(
        self,
        *args,
        extra_configs: Optional[list] = None,
        ignore_known_configs: bool = False,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self.config = self._init_config(extra_configs, ignore_known_configs)
        self._ignoring_known_configs = ignore_known_configs
        self.saved_carousels: list[CarouselSpec] = []
        self.open_carousels: list[Carousel] = []
        self.setQuitOnLastWindowClosed(False)
        QImageReader().setAllocationLimit(0)

        self._read_carousels_from_config()
        self.prefs_window = PrefsWindow(self.config)
        self._ui_state = QSettings("ExitCodeOne", "Kaleidoshow")
        self._menu_bar = KaleidoshowMenuBar(self)

        self._photoset_loader = PhotosetLoader()

    def add_carousel(self, spec: CarouselSpec) -> None:
        log.debug(f"Adding carousel spec to saved carousels: ({type(spec)}){spec}")
        # Remove carousel from existing list if there is one by that name
        for c in self.saved_carousels:
            if c.name == spec.name:
                self.saved_carousels.remove(c)
        self.saved_carousels.append(spec)

    def open_carousel(self, spec: CarouselSpec) -> None:
        spec = spec.updated_with_defaults(self.config.carousel_settings)
        shuffle = spec.mode == "shuffled"

        photosets = []
        for window_spec in spec.paths:
            photoset = self._photoset_loader.load_directory(
                window_spec.path,
                shuffled=shuffle,
                recursive=spec.recursive,
                exclude_pattern=spec.exclude_pattern,
                include_pattern=spec.include_pattern,
            )
            photosets.append((window_spec, photoset))

        try:
            carousel = self.build_carousel(photosets, spec)
            self.open_carousels.append(carousel)
            carousel.carousel_closed.connect(self._carousel_closed)
            carousel.show()
            if spec.start_on_load:
                carousel.start()
        except (PhotosetEmptyError, NoWindowsToCreateError) as e:
            log.error(e)

    def open_carousel_through_dialog(self) -> None:
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.Directory)  # type: ignore
        last_open = str(self.read_ui_state("LastOpenedLocation", defaultValue=""))
        photodir = dialog.getExistingDirectory(dir=last_open)
        if photodir:  # Cancelling returns empty string
            log.info(f"New Carousel requested through Open dialog: {photodir}")
            self._set_ui_state("LastOpenedLocation", photodir)
            self.save_recent(photodir)
            builder = CarouselSpecBuilder()
            self.open_carousel(builder.build_from_paths(photodir))

    def open_through_recents_menu(self, pth: str) -> None:
        spec = CarouselSpecBuilder().build_from_paths(pth)
        self.open_carousel(spec)
        self.save_recent(pth)

    def save_carousel(self, carousel: Carousel):
        log.debug(f"Initiating save for carousel: {carousel.name}")
        name, ok = QInputDialog.getText(
            None,
            "Save Carousel",
            "Enter a name for the carousel:",
            text=carousel.name,
        )
        if not ok or not name:
            log.debug("Save cancelled.")
            return

        savepath = APPDIR / f"{name}.crsl"
        log.info(f"Saving carousel '{carousel.name}' to: {savepath}")
        windows = []
        for w in carousel.windows:
            path = w.photoset.root_directory
            position = list(w.geometry().getRect())
            windows.append({"path": path, "position": position})
        outconfig = KaleidoshowConfig(
            name=name,
            paths=windows,
            interval=carousel.interval,
            mode=carousel.windows[0].photoset.mode,
            show_tooltips=carousel.windows[0].show_tooltips,
        )
        outconfig.save(filepath=savepath)
        spec = CarouselSpecBuilder().build_from_dict(outconfig)
        self.add_carousel(spec)
        # Put at the end in case of failure
        carousel.name = name

    def delete_carousel(self, carousel: CarouselSpec) -> None:
        self.saved_carousels.remove(carousel)
        for f in self.config.search_known_dirs_for_carousel_files():
            # File names do not have to match the carousel name, so we have to
            # look inside the file to get the name
            c = KaleidoshowConfig.from_carousel_file(f)
            if c.name == carousel.name:
                f.unlink()

    def build_carousel(
        self, photosets: list[tuple[WindowPathSpec, Photoset]], spec: CarouselSpec
    ) -> Carousel:
        if len(photosets) == 0:
            msg = f"Cannot create Carousel '{spec.name}' with an empty Photoset list."
            raise NoWindowsToCreateError(msg)
        carousel = Carousel(
            spec.name,
            interval=spec.interval,
        )
        for window_spec, photoset in photosets:
            window = DisplayWindow(
                photoset,
                show_tooltips=spec.show_tooltips,
                carousel=carousel,
            )
            if window_spec.position is not None:
                window.setGeometry(QRect(*window_spec.position))
            carousel.add_window(window)
        return carousel

    def _carousel_closed(self, carousel: Carousel) -> None:
        msg = f"All windows for Carousel '{carousel.name}' closed. Removing references."
        log.info(msg)
        self.open_carousels.remove(carousel)
        log.debug(f"Remaining open Carousels: {self.open_carousels}")

    def _init_config(
        self, extra_configs: Optional[list[PathLike]], ignore_known_configs: bool
    ):
        config = KaleidoshowConfig.with_defaults()
        if ignore_known_configs:
            log.info("--ignore-known-configs: True. Continuing with defaults only...")
        else:
            config.load_known_configs()
        if extra_configs is not None:
            for c in extra_configs:
                config.update_from_file(c)
        return config

    def _read_carousels_from_config(self) -> None:
        builder = CarouselSpecBuilder()
        for _, c in self.config.carousels.items():
            self.add_carousel(builder.build_from_dict(c))

    def save_recent(self, pth: str) -> None:
        recents: list[str] = self.read_ui_state("Recents", defaultValue=list())
        if pth in recents:
            # We'll move it back to the top of the list
            recents.remove(pth)
        recents.insert(0, pth)
        self._set_ui_state("Recents", recents[:10])

    def clear_recents(self) -> None:
        self._set_ui_state("Recents", [])

    def read_ui_state(self, key, defaultValue="null"):
        """
        Wrapper for reading QSettings.

        When `ignore_known_configs` is passed as True to create the Kaleidoshow
        instance, Kaleidoshow does not read any settings from the user's
        %APPDATA% folder. It is expected that the ignore mode will only
        be used during testing sessions and there will be no need to read UI
        settings.

        This will simply return the default value, or raise an
        error. If calling code is expecting an error free flow when Kaleidoshow
        is not using configs, it will need to provide a default to return.
        """

        # defaultValue could actually be `None` in some circumstances. `null`
        # is defined as a placeholder where `None` would usually be used to
        # indicate that no value has been received.
        if self._ignoring_known_configs:
            if defaultValue == "null":
                raise
            else:
                return defaultValue

        if defaultValue == "null":
            return self._ui_state.value(key)
        else:
            return self._ui_state.value(key, defaultValue=defaultValue)

    def _set_ui_state(self, key, value):
        """
        Wrapper for writing QSettings.

        When `ignore_known_configs` is passed as True to create the Kaleidoshow
        instance, this acts as a black hole. Nothing is saved, even internally
        to the runtime instance. It is expected that the ignore mode will only
        be used during testing sessions and there will be no need to save UI
        settings.
        """
        if self._ignoring_known_configs:
            return

        self._ui_state.setValue(key, value)

    def event(self, event):
        def _is_pytest_argument(url: str) -> bool:
            """
            Hacky little thing to make sure Kaleidoshow isn't handling
            erroneous FileOpenEvents. When running pytest, Kaleidoshow receives
            a FileOpenEvent from the OS based on the pytest command used to
            start it. These are obviously not valid image folders and if they
            were, we wouldn't want to add them to the known carousels. This
            validates that `pytest` is not in the url and that the url is not
            part of the project folder, but does allow it, if the url points to
            the `test/test-images` directory.
            """
            proj_root = environment.APP_ROOT.parent
            test_images_dir = proj_root / "test/test-images"
            return "pytest" in url or (
                str(proj_root) in url and str(test_images_dir) not in url
            )

        if isinstance(event, QFileOpenEvent):
            url = event.url().toLocalFile()
            if _is_pytest_argument(url):
                return super().event(event)
            msg = f"New Carousel requested through icon drop or system request: {url}"
            log.info(msg)
            builder = CarouselSpecBuilder()
            self.open_carousel(builder.build_from_paths(url))
            self.save_recent(url)
        return super().event(event)
