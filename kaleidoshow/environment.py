import sys
from pathlib import Path

# https://stackoverflow.com/a/42615559
# Path resolution is different based on whether run as a Standalone app, or
# Python script.
if getattr(sys, 'frozen', False):
    APP_ROOT = Path(sys._MEIPASS)
else:
    APP_ROOT = Path(__file__).resolve().parent
