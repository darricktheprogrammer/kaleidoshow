import logging
from pathlib import Path
from functools import partial
from typing import Callable

from PySide6.QtWidgets import QMenuBar, QMenu, QApplication, QInputDialog
from PySide6.QtGui import QAction, QClipboard

from .input import int_or_float
from .styles import style_for_platform
from ..photoset import PhotosetLoader

log = logging.getLogger(__name__)


class ActionMenu(QMenu):
    def add_menu_item(self, name: str, func: Callable, checked: bool = False) -> None:
        action = QAction(name, parent=self)
        if checked:
            action.setCheckable(True)
            action.setChecked(True)
        action.triggered.connect(func)  # type: ignore
        self.addAction(action)

    def add_disabled_menu_item(self, name: str) -> None:
        action = QAction(name, self)
        action.setEnabled(False)
        self.addAction(action)


# Context Menu menus
###############################################################################


class IntervalMenu(ActionMenu):
    def __init__(self, window, parent=None):
        super().__init__("Interval", parent=parent)
        self._window = window

        current_interval = window.carousel.interval
        intervals = QApplication.instance().config.application_settings.intervals
        if current_interval not in intervals:
            # Custom value set by the user at runtime
            intervals = intervals + [int_or_float(current_interval)]

        for interval in intervals:
            self.add_menu_item(
                str(interval),
                partial(window.user_interval_update.emit, interval),
                checked=current_interval == interval,
            )

        self.addSeparator()
        self.add_menu_item("Custom...", self._custom_interval)

    def _custom_interval(self) -> None:
        interval, ok = QInputDialog.getDouble(
            self._window,
            "Kaleidoshow",
            "Interval:",
            value=self._window.carousel.interval,
            decimals=2,
            minValue=0.01,
        )

        if not ok:
            return
        self._window.user_interval_update.emit(interval)


class OrderMenu(ActionMenu):
    def __init__(self, window, parent=None):
        super().__init__("Order", parent=parent)
        self._window = window

        is_shuffled = window.photoset.mode == "shuffled"
        shuffle_display = "Shuffled (click to reshuffle)" if is_shuffled else "Shuffled"

        self.add_menu_item(
            "Ordered",
            partial(self._convert_photoset_order, "ordered"),
            checked=not is_shuffled,
        )
        self.add_menu_item(
            shuffle_display,
            partial(self._convert_photoset_order, "shuffled"),
            checked=is_shuffled,
        )

    def _convert_photoset_order(self, order: str) -> None:
        photoset = self._window.photoset
        if order == "ordered" and photoset.mode == "ordered":
            log.warn(f"Cannot convert photoset order to '{order}'. It already is.")
            return

        if order == "ordered":
            prev_filelist = photoset._filelist
            self._window.photoset = photoset.convert_to_ordered_photoset()
        elif order == "shuffled":
            prev_filelist = photoset._filelist
            self._window.photoset = photoset.convert_to_shuffled_photoset()

        log.info(f"Photoset '{photoset.name}' order converted to '{order}'")
        log.debug("Previous filelist:")
        for f in prev_filelist:
            log.debug(str(f))
        log.debug("")
        log.debug("new filelist:")
        for f in self._window.photoset._filelist:
            log.debug(str(f))


class ContextMenu(ActionMenu):
    def __init__(self, window):
        super().__init__(parent=window)
        self._window = window
        interval_menu = IntervalMenu(window, parent=self)
        order_menu = OrderMenu(window, parent=self)
        self._set_styles([interval_menu, order_menu])

        next_func = partial(window.user_next_request.emit, window, True)
        prev_func = partial(window.user_previous_request.emit, window, True)
        copy_path = partial(QClipboard().setText, str(window.photoset.current_file))
        copy_filename = partial(QClipboard().setText, window.photoset.current_file.name)
        copy_basename = partial(QClipboard().setText, window.photoset.current_file.stem)

        self.add_menu_item("Next Image", next_func)
        self.add_menu_item("Previous Image", prev_func)
        self.add_menu_item("Start", window.start)
        self.add_menu_item("Stop", window.stop)
        self.addSeparator()
        self.add_menu_item("Start Carousel", window.carousel_start.emit)
        self.add_menu_item("Stop Carousel", window.carousel_stop.emit)
        self.addSeparator()
        self.addMenu(interval_menu)
        self.addSeparator()
        self.addMenu(order_menu)
        self.add_menu_item("Refresh", self._reload_photoset)
        self.addSeparator()
        self.add_menu_item("Copy path", copy_path)
        self.add_menu_item("Copy filename", copy_filename)
        self.add_menu_item("Copy basename", copy_basename)
        self.addSeparator()
        self.add_menu_item("Close", window.close)

    def _set_styles(self, submenus: list[ActionMenu]) -> None:
        self.setMinimumWidth(160)
        style = style_for_platform()
        self.setStyleSheet(style.css)

    def _reload_photoset(self):
        photoset = self._window.photoset
        log.info(f"Reloading images from photoset: {photoset.root_directory}")

        # Default settings can be overridden in a saved carousel. Check if it's
        # saved first, then use the default if not.
        recursive = None
        exclude_pattern = None
        include_pattern = None
        app = QApplication.instance()
        carousel = self._window.carousel
        for c in app.saved_carousels:
            if carousel.name == c.name:
                # All properties are optional in a spec.
                recursive = c.get("recursive", None)
                exclude_pattern = c.get("exclude_pattern", None)
                include_pattern = c.get("include_pattern", None)
                break

        if recursive is None:
            recursive = app.config.carousel_settings.recursive
        if exclude_pattern is None:
            exclude_pattern = app.config.carousel_settings.exclude_pattern
        if include_pattern is None:
            include_pattern = app.config.carousel_settings.include_pattern

        shuffled = photoset.mode == "shuffled"
        reloaded = PhotosetLoader().load_directory(
            Path(photoset.root_directory),
            shuffled=shuffled,
            recursive=recursive,
            exclude_pattern=exclude_pattern,
            include_pattern=include_pattern,
        )
        self._window.photoset = reloaded
        log.debug(reloaded._filelist)


# Menu Bar menus
###############################################################################


class RecentsMenu(ActionMenu):
    def __init__(self, app, *args, **kwargs):
        super().__init__("Open Recent", *args, **kwargs)
        self._app = app
        self.aboutToShow.connect(self.update_list)

    def update_list(self) -> None:
        self.clear()
        recents = self._app.read_ui_state("Recents", defaultValue=list())
        if not recents:
            self.add_disabled_menu_item("(No Recent Files)")
            return

        for photodir in recents:
            func = partial(self._app.open_through_recents_menu, photodir)
            self.add_menu_item(photodir, func)

        self.addSeparator()
        self.add_menu_item("Clear Recents", self._app.clear_recents)


class FileMenu(ActionMenu):
    def __init__(self, app, *args, **kwargs):
        super().__init__("File", *args, **kwargs)
        self.add_menu_item("New Carousel...", app.open_carousel_through_dialog)
        self.addMenu(RecentsMenu(app))

        # On Windows/Linux the Preferences item usually goes in the Edit Menu,
        # but there are no other menu items in the Edit Menu. When running on
        # macOS `Preferences` are moved to the $APPNAME menu, leaving the Edit
        # Menu empty and causing a runtime error:
        # 'NSInternalInconsistencyException', reason: 'Invalid parameter not
        # satisfying: index >= 0'
        self.add_menu_item("Preferences", app.prefs_window.show)


class SavedCarouselMenu(ActionMenu):
    def __init__(self, app, *args, **kwargs):
        super().__init__("Saved Carousels", *args, **kwargs)
        self._app = app
        self.aboutToShow.connect(self.update_list)

        # The menu doesn't show in the menu bar until it is populated for the
        # first time.
        self.update_list()

    def update_list(self) -> None:
        self.clear()
        if not self._app.saved_carousels:
            self.add_disabled_menu_item("(No Carousels loaded)")
            return

        for c in sorted(self._app.saved_carousels, key=lambda c: c.name.lower()):
            self.add_menu_item(f"Open: {c.name}", partial(self._app.open_carousel, c))
        self.addSeparator()
        for c in sorted(self._app.saved_carousels, key=lambda c: c.name.lower()):
            self.add_menu_item(
                f"Remove: {c.name}", partial(self._app.delete_carousel, c)
            )


class OpenCarouselsMenu(ActionMenu):
    def __init__(self, app, *args, **kwargs):
        super().__init__("Active Carousels", *args, **kwargs)
        self._app = app
        self.aboutToShow.connect(self.update_list)

        # The menu doesn't show in the menu bar until it is populated for the
        # first time.
        self.update_list()

    def update_list(self) -> None:
        self.clear()
        if not self._app.open_carousels:
            self.add_disabled_menu_item("(No Active Carousels)")
            return

        for c in sorted(self._app.open_carousels, key=lambda c: c.name.lower()):
            func = partial(self._app.save_carousel, c)
            self.add_menu_item(f"Save: {c.name}", func)
        self.addSeparator()
        for c in sorted(self._app.open_carousels, key=lambda c: c.name.lower()):
            self.add_menu_item(f"Close: {c.name}", c.close)


class KaleidoshowMenuBar(QMenuBar):
    """The Main menu bar for the Kaleidoshow app."""

    def __init__(self, app, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.addMenu(FileMenu(app))
        self.addMenu(SavedCarouselMenu(app))
        self.addMenu(OpenCarouselsMenu(app))
