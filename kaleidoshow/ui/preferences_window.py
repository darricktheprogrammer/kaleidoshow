from PySide6.QtWidgets import (
    QWidget,
    QMessageBox,
    QGridLayout,
    QLabel,
    QLineEdit,
    QDialogButtonBox,
    QRadioButton,
    QButtonGroup,
    QHBoxLayout,
    QCheckBox,
)

from PySide6.QtCore import Qt
from PySide6.QtGui import QShortcut, QKeySequence

from .input import int_or_float

# Short names for long Enums
AlignLeft = Qt.AlignmentFlag.AlignLeft
AlignRight = Qt.AlignmentFlag.AlignRight
OkButton = QDialogButtonBox.StandardButton.Ok
CancelButton = QDialogButtonBox.StandardButton.Cancel


class PrefsWindow(QWidget):
    """Show and manage the active user preferences."""

    interval_tooltip = (
        "Time in seconds each window display an image."
        " Carousels with more than one window will swap images faster"
        " than the given value, but each window is displayed for this long"
    )
    interval_opts_tooltip = "Convenience interval options found in the context menu."
    ordered_mode_tooltip = "Display images alphabetically as found in the file system."
    exclude_pattern_tooltip = (
        "Exclude images matching this pattern from being displayed"
    )
    include_pattern_tooltip = (
        "Only display images matching this pattern"
    )
    shuffled_mode_tooltip = "Display images in random order, without duplicates."
    recursive_tooltip = (
        "Search all subfolders for images."
        " The default is to only load images from the top-level folder."
    )
    start_onload_tooltip = "Start carousels immediately when they are opened."
    show_tooltips_tooltip = (
        "Show tooltips with image information such as"
        " filename, filepath, and the carousel the image belongs to"
        " when hovering over a window."
    )

    def __init__(self, config):
        super().__init__()
        self.setWindowTitle("Kaleidoshow Settings")

        self._config = config
        self._layout = QGridLayout()
        self._layout.setContentsMargins(35, 20, 35, 20)
        self.setLayout(self._layout)
        self._build_ui_elements()
        self._set_shortcuts()

    def _build_ui_elements(self):
        self._interval = self._add_interval()
        self._interval_options = self._add_interval_options()
        self._include_pattern = self._add_include_pattern()
        self._exclude_pattern = self._add_exclude_pattern()
        self._mode = self._add_mode()
        self._recursive = self._add_recursive()
        self._start_on_load = self._add_start_on_load()
        self._show_tooltips = self._add_show_tooltips()
        self._add_buttons()

    def _set_shortcuts(self):
        cancel_keys = QShortcut(QKeySequence.Cancel, self)
        cancel_keys.activated.connect(self.reject)
        close_window_keys = QShortcut(QKeySequence.Close, self)
        close_window_keys.activated.connect(self.reject)
        accept_keys = QShortcut(QKeySequence.InsertParagraphSeparator, self)
        accept_keys.activated.connect(self.accept)

    def _update_ui_values(self, config):
        app_settings = config.application_settings
        carousel_settings = config.carousel_settings

        self._interval.setText(str(carousel_settings.interval))
        interval_str = [str(i) for i in app_settings.intervals]
        self._interval_options.setText(", ".join(interval_str))
        self._exclude_pattern.setText(carousel_settings.exclude_pattern)
        self._include_pattern.setText(carousel_settings.include_pattern)
        for button in self._mode.buttons():
            button.setChecked(button.text().lower() == carousel_settings.mode)
        self._recursive.setChecked(carousel_settings.recursive)
        self._start_on_load.setChecked(carousel_settings.start_on_load)
        self._show_tooltips.setChecked(carousel_settings.show_tooltips)

    def _add_interval(self) -> QLineEdit:
        interval_frame = QLineEdit()
        interval_frame.setMaxLength(6)
        interval_frame.setMaximumWidth(60)
        self._add_preference("Interval:", interval_frame, tooltip=self.interval_tooltip)
        return interval_frame

    def _add_interval_options(self) -> QLineEdit:
        interval_options = QLineEdit()
        interval_options.setMinimumWidth(int(self.width() / 2))
        self._add_preference(
            "Interval menu options:",
            interval_options,
            tooltip=self.interval_opts_tooltip,
        )
        return interval_options

    def _add_exclude_pattern(self) -> QLineEdit:
        exclude_pattern = QLineEdit()
        exclude_pattern.setMinimumWidth(int(self.width() / 2))
        self._add_preference(
            "Exclude Pattern:",
            exclude_pattern,
            tooltip=self.exclude_pattern_tooltip,
        )
        return exclude_pattern

    def _add_include_pattern(self) -> QLineEdit:
        include_pattern = QLineEdit()
        include_pattern.setMinimumWidth(int(self.width() / 2))
        self._add_preference(
            "Include Pattern:",
            include_pattern,
            tooltip=self.include_pattern_tooltip,
        )
        return include_pattern

    def _add_mode(self) -> QButtonGroup:
        mode_group = QButtonGroup()
        radio_layout = QHBoxLayout()
        ordered_radio = QRadioButton("Ordered")
        shuffled_radio = QRadioButton("Shuffled")
        ordered_radio.setToolTip(self.ordered_mode_tooltip)
        shuffled_radio.setToolTip(self.shuffled_mode_tooltip)
        mode_group.addButton(ordered_radio)
        mode_group.addButton(shuffled_radio)
        radio_layout.addWidget(ordered_radio)
        radio_layout.addWidget(shuffled_radio)
        self._add_preference("Image Order:", radio_layout)
        return mode_group

    def _add_recursive(self) -> QCheckBox:
        recursive_check = QCheckBox("Load images from subfolders")
        self._add_preference("", recursive_check, tooltip=self.recursive_tooltip)
        return recursive_check

    def _add_start_on_load(self) -> QCheckBox:
        start_on_load_check = QCheckBox("Start Carousels when opened")
        self._add_preference("", start_on_load_check, tooltip=self.start_onload_tooltip)
        return start_on_load_check

    def _add_show_tooltips(self) -> QCheckBox:
        show_tooltips_check = QCheckBox("Show Tooltips")
        self._add_preference(
            "", show_tooltips_check, tooltip=self.show_tooltips_tooltip
        )
        return show_tooltips_check

    def _add_buttons(self):
        button_box = QDialogButtonBox(OkButton | CancelButton)
        self._layout.addWidget(button_box, self._layout.rowCount() + 1, 1)
        button_box.button(OkButton).setDefault(True)
        button_box.accepted.connect(self.accept)
        button_box.rejected.connect(self.reject)

    def _add_preference(self, label, widget, tooltip=None):
        next_row = self._layout.rowCount() + 1
        if tooltip is not None:
            widget.setToolTip(tooltip)
        self._layout.addWidget(QLabel(label), next_row, 0, alignment=AlignRight)
        if isinstance(widget, QWidget):
            self._layout.addWidget(widget, next_row, 1, alignment=AlignLeft)
        else:
            self._layout.addLayout(widget, next_row, 1, alignment=AlignLeft)

    def accept(self):
        try:
            interval = int_or_float(self._interval.text())
            interval_opts = [
                int_or_float(n.strip())
                for n in self._interval_options.text().split(",")
            ]
        except ValueError:
            # Title currently doesn't show on macOS. This is a known bug in Qt.
            # https://bugreports.qt.io/browse/QTBUG-74947
            QMessageBox.about(
                None,
                "Preference Update Error",
                "Intervals must be a number",
            )
            return
        mode = self._mode.checkedButton().text().lower()
        recursive = self._recursive.isChecked()
        start_on_load = self._start_on_load.isChecked()
        show_tooltips = self._show_tooltips.isChecked()
        self.close()

        self._config.carousel_settings.interval = interval
        self._config.application_settings.intervals = interval_opts
        self._config.carousel_settings.mode = mode
        self._config.carousel_settings.exclude_pattern = self._exclude_pattern.text()
        self._config.carousel_settings.include_pattern = self._include_pattern.text()
        self._config.carousel_settings.recursive = recursive
        self._config.carousel_settings.start_on_load = start_on_load
        self._config.carousel_settings.show_tooltips = show_tooltips
        self._config.save()

    def reject(self):
        self.close()

    def show(self):
        self._update_ui_values(self._config)
        super().show()
