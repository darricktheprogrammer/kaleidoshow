import logging

from PySide6.QtCore import Qt, QTimer, QRect, QPoint
from PySide6.QtGui import QPixmap
from PySide6.QtWidgets import QLabel


log = logging.getLogger(__name__)


class ImageWindow(QLabel):
    _style = "QWidget#ImageWindow {background-color:rgba(0, 0, 0, .10);}"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setObjectName("ImageWindow")
        self.setStyleSheet(self._style)
        self._img = None
        self.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.setWindowFlags(
            self.windowFlags()
            | Qt.FramelessWindowHint  # type: ignore
            | Qt.NoDropShadowWindowHint  # type: ignore
        )
        self._resize_timer = QTimer()
        self._resize_timer.setSingleShot(True)
        self._resize_timer.timeout.connect(self._resize_complete)
        self._handling_event = False
        self._resizing = False

    def _scaled_pixmap(self) -> QPixmap:
        return self._img.scaled(
            self.size(),
            aspectMode=Qt.AspectRatioMode.KeepAspectRatio,
            mode=Qt.TransformationMode.SmoothTransformation,
        )

    def _resize_pixmap(self) -> None:
        self._resizing = True
        super().setPixmap(self._scaled_pixmap())
        self._resizing = False

    def _resize_started(self) -> None:
        self._resizing = True
        self._wait_for_mouse_release()

    def _resize_complete(self) -> None:
        img_true_geometry = self._true_pixmap_geometry()
        self.setGeometry(img_true_geometry)
        g = self.geometry()
        self._resizing = False

        msg = f"Resized: {g.getRect()} ({g.width()}x{g.height()}) Center: {g.center()}"
        log.debug(msg)

    def _wait_for_mouse_release(self) -> None:
        wait = 750
        if self._resize_timer.isActive():
            self._resize_timer.stop()
            self._resize_timer.start(wait)
            return

        self._resize_timer.start(wait)

    def _true_pixmap_geometry(self) -> QRect:
        """
        Find the true global geometry of the pixmap.

        Unlike child Widgets, a QLabel's pixmap always returns (0, 0) for the
        topLeft corner of its geometry. Even a vertically centered 100x100
        pixmap in a 100x500 Qlabel will return (0, 0) for
        `label.pixmap.geometry().topLeft()`.

        This method calcuates the global position of the pixmap using the
        window's geometry and the pixmap's size.

        Returns:
            QRect
        """
        w: int
        h: int
        w, h = self.pixmap().size().toTuple()
        topleft = self.geometry().center() - QPoint(w / 2, h / 2)
        bottomright = self.geometry().center() + QPoint(w / 2, h / 2)
        return QRect(topleft, bottomright)

    def _center_point(self) -> tuple[int, int]:
        # Casting to int causes the image to drift to the left. The `round`
        # function uses banker's rounding which over time evens out any
        # inconsistencies and keeps the image in place.
        return round(self.x() + self.width() / 2), round(self.y() + self.height() / 2)

    def setPixmap(self, pix) -> None:
        center_x, center_y = self._center_point()
        self._img = pix
        scaled = self._img.scaledToHeight(
            self.height(), mode=Qt.TransformationMode.SmoothTransformation
        )
        x = center_x - scaled.width() / 2
        y = center_y - scaled.height() / 2
        self.move(x, y)
        super().setPixmap(scaled)
        self.adjustSize()

    def resizeEvent(self, event):
        if self._handling_event:
            return

        self._handling_event = True
        self._resize_pixmap()
        self._resize_started()
        self._handling_event = False

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.offset = event.position()
        else:
            super().mousePressEvent(event)

    def mouseMoveEvent(self, event):
        if self.offset is not None and event.buttons() == Qt.LeftButton:
            new_pos = self.pos() + event.position().toPoint() - self.offset.toPoint()
            self.move(new_pos)
        else:
            super().mouseMoveEvent(event)

    def mouseReleaseEvent(self, event):
        self.offset = None
        super().mouseReleaseEvent(event)
