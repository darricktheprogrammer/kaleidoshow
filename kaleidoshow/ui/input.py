from typing import Union


def int_or_float(n: Union[str, int, float]) -> Union[int, float]:
    nfloat = float(n)
    if nfloat == int(nfloat):
        return int(nfloat)
    return nfloat
