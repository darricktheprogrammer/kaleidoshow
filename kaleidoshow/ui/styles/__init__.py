import platform

from pkg_resources import parse_version

from .platform_styles import BaseStyle, macOSStyle


__all__ = ["macOSStyle"]


def style_for_platform():
    # Apple changed their menu style with macOS 11 (Big Sur). QT currently only
    # supports the older style, so create the new style manually, otherwise
    # fall back to QT's styling
    if platform.system() == "Darwin":
        new_style_version = parse_version("11.0")
        current_version = parse_version(platform.mac_ver()[0])
        if current_version > new_style_version:
            return macOSStyle()
    return BaseStyle()
