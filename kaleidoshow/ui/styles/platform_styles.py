import plistlib
from pathlib import Path

from PySide6.QtGui import QPalette

from kaleidoshow import environment


STYLE_DIR = environment.APP_ROOT / "ui/styles"


class BaseStyle:
    @property
    def css(self):
        return ""


class macOSStyle(BaseStyle):
    accent_colors = {
        -1: "rgb(152, 152, 152)",  # graphite
        0: "rgb(224, 57, 63)",  # red
        1: "rgb(247, 130, 27)",  # orange
        2: "rgb(255, 199, 37)",  # yellow
        3: "rgb(98, 185, 71)",  # green
        4: "rgb(4, 122, 255)",  # blue
        5: "rgb(149, 62, 150)",  # purple
        6: "rgb(247, 79, 159)",  # pink
    }

    def __init__(self):
        super().__init__()
        self.sysprefs = Path.home() / "Library/Preferences/.GlobalPreferences.plist"
        self.os_dir = STYLE_DIR / "macOS"

    @property
    def css(self):
        css = self._load_css("base.css")
        css += self._load_css("light.css" if self.is_light_mode() else "dark.css")
        css = css.replace("@accent-color", self.accent_rgb())
        css = css.replace("@checkmark-path", str(self.os_dir))
        return css

    def is_light_mode(self):
        # Background colors returned by palette:
        # light mode: 236
        # dark mode: 50
        return QPalette().color(QPalette.Active, QPalette.Window).lightness() == 236

    def accent_rgb(self):
        # The accent color is not included in the palette, and without writing
        # a C++ extension, I don't think there's a way to subscribe to UI
        # change events not already provided by Qt. So, we're left with reading
        # the global config file, which means there can be a delay of 5-10
        # seconds before the accent color is changed if the user changes their
        # accent color while Kaleidoshow is running.
        with open(self.sysprefs, "rb") as plist:
            sysprefs_dict = plistlib.load(plist)
        return self.accent_colors[sysprefs_dict.get("AppleAccentColor", 4)]

    def _load_css(self, fn: str) -> str:
        return (self.os_dir / fn).read_text()
