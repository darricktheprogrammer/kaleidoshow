"""
Delete all of the saved user preferences.

Helpful when developing and trying to determine if preferences are saved,
loaded and displayed properly without dirtying the space with existing
settings.
"""

from PySide6.QtCore import QSettings


settings = QSettings("ExitCodeOne", "Kaleidoshow")
settings.clear()
