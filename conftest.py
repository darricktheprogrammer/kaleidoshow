from pathlib import Path

import pytest

from kaleidoshow.app import Kaleidoshow

APP = None


@pytest.fixture(scope="function")
def qapp_cls():
    global APP
    if APP is None:
        APP = Kaleidoshow(ignore_known_configs=True)
    yield APP
    APP.instance().shutdown()
    APP = None


@pytest.fixture
def test_root():
    return Path(__file__).resolve().parent / "test"


@pytest.fixture
def project_root():
    return Path(__file__).resolve().parent
