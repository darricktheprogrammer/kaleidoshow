Kaleidoshow
========
Kaleidoshow is a slideshow viewer heavily inspired by [Photostickies](https://www.devontechnologies.com/apps/freeware) by DEVONtechnologies. Mix and match folders of images to create a multi-window image display.

![Kaleidoshow feature image](docs/img/kaleidoshow-hero.png "Kaleidoshow feature image")


Features
--------
* Frameless windows for unobtrusive viewing
* Create window groups (called Carousels) for displaying multiple sets of images
* Fast loading time, even on a large number of images
* Simple to start/stop windows, start/stop entire Carousels, or change slideshow speed
* Save Carousels for later viewing
* Supports most common image formats including bmp, gif[^1], heic, jpeg, png, svg, tif, and webp
* Cross-platform[^2]. Works on macOS, Windows, and Linux
* Open Source (MIT Licensed)


Limitations
-----------
* You can only view folders of images, not a single image
* No viewing of web-based images
* Unconventional image resizing


User Guide
----------
See the [full documentation](https://darricktheprogrammer.gitlab.io/kaleidoshow/)


Attribution
-----------
Kaleidoshow is built with Python using the PySide6 QT bindings. QT is licensed under the LGPLv3.

Kaleidoshow also uses images released under the public domain for documentation and testing. More details can be found in the [attribution file](test/test-images/attribution.txt).



[^1]: Animated gifs are not currently supported. Only the first frame will be shown.
[^2]: Kaleidoshow is currently in Alpha and has only been tested on macOS.
