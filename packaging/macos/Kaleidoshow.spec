# -*- mode: python ; coding: utf-8 -*-

from kaleidoshow import __version__

block_cipher = None


a = Analysis(
    ["../../kaleidoshow/kaleidoshow-cli.py"],
    pathex=[],
    binaries=[],
    datas=[
        ("../../kaleidoshow/defaults.yml", "."),
        ("../../kaleidoshow/assets", "./assets"),
        ("../../kaleidoshow/ui/styles/macOS", "./ui/styles/macOS"),
    ],
    hiddenimports=[],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False,
)
pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

exe = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name="Kaleidoshow",
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    console=False,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
)
coll = COLLECT(
    exe,
    a.binaries,
    a.zipfiles,
    a.datas,
    strip=False,
    upx=True,
    upx_exclude=[],
    name="Kaleidoshow",
)
app = BUNDLE(
    coll,
    name="Kaleidoshow.app",
    icon="./kaleidoshow.icns",
    bundle_identifier="com.exitcodeone.kaleidoshow",
    info_plist={
        "CFBundleDocumentTypes": [
            {
                "CFBundleTypeName": "Folder",
                "CFBundleTypeRole": "Viewer",
                "LSItemContentTypes": ["public.folder"],
                "LSHandlerRank": "Alternate",
            }
        ],
        "CFBundleShortVersionString": __version__,
    },
)
