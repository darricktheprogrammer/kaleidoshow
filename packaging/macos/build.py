"""Build a self-signed macOS app for running on the local machine."""
import shutil
from pathlib import Path

import PyInstaller.__main__

MACOS_ROOT = Path(__file__).resolve().parent

buildpath = MACOS_ROOT / "build"
distpath = MACOS_ROOT / "dist"
args = [
    str(MACOS_ROOT / "Kaleidoshow.spec"),
    "--workpath",
    str(buildpath),
    "--distpath",
    str(distpath),
]

PyInstaller.__main__.run(args)

# We are only interested in the final executables. Remove any intermediate
# files/folders created in the process.
shutil.rmtree(buildpath)
shutil.rmtree(distpath / "Kaleidoshow")
