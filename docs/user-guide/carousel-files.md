Carousel Files
==============
Carousel files (`.crsl`) are Kaleidoshow's file format for saving the state of a Carousel. These are written when a Carousel is saved through the main menu, and read when a saved Carousel is opened.

These are remaining undocumented for now, but can be read/modified using the same instructions as the [Configuration File](configuration-file.md). When started, Kaleidoshow looks in the same locations for any `.crsl` files and loads them into the `Saved Carousels` menu.
