Saving/Loading Carousels
========================
Kaleidoshow has the ability to save Carousels and open them again for later viewing.


Saving a Carousel
-----------------
To save a Carousel, choose <code>Active Carousels > Save: _CarouselName_</code> from the menu bar. If you are saving a new Carousel that has not been saved previously, you will need to look for its [generated name](concepts.md#carousel-names)

After choosing the menu option, you will be prompted with a dialog box to give your Carousel a name. Simply type the name in the box and click `Ok`. 

!!! warning
	If a Carousel already exists with the given name, it will be overwritten with no prompting.

![Active Carousels Menu](../img/active-carousels-menu.png "Active Carousels Menu")


When you save a Carousel, the location of each window is saved, along with all of the Carousel's properties. This means that saved Carousels maintain all of your current Kaleidoshow preferences. If you change your [default preferences](preferences.md), they will not be honored by any saved Carousels, even if you did not explicitly change any settings in the Carousel.


Loading a Carousel
------------------
To open a saved Carousel, choose <code>Saved Carousels > Open: _CarouselName_</code> from the menu bar.

![Saved Carousels Menu](../img/saved-carousels-menu.png "Saved Carousels Menu")


Deleting a Saved Carousel
-------------------------
To delete a saved Carousel, choose <code>Saved Carousels > Remove: _CarouselName_</code> from the menu bar.
