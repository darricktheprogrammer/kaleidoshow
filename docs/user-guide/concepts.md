Carousels
---------
Carousels are the heart of Kaleidoshow. They are meant to make managing a slideshow with multiple windows simple but powerful.

A Carousel is a group of windows. Grouping multiple windows into a single Carousel makes it quick and easy to control all of the windows at one time. When you start or stop a Carousel, or change its speed, all windows within the Carousel will be updated accordingly.


Photosets
---------
A Photoset represents the images within a single folder. Depending on your settings, a photoset may include images from a folder and all of its subfolders, or only contain images in the top-level of the folder. No matter your options, a photoset will always be representative of a single folder.


Image Window
------------
An image window displays the images from a photoset. There is no way to display images from multiple folders in the same window. To display images from more than one folder at the same time, you must open two separate windows, each with their own photoset.


More on Carousels
-----------------

A Carousel splits its play speed across the windows it controls evenly. For example, a Carousel with two windows, set to an interval of 10 will display a new image every 5 seconds. Each window will display its image for the full 10 seconds.

Except for the power of your computer, there is no limit to how many Carousels you can have open, or how many windows you have open in a Carousel. There are infinite possibilities to the slideshows you can create.

![Carousel Demo](../img/carousel-demo.gif "Carousel Demo")

In the image above, there are 3 windows. The top window is a single-window Carousel with an interval of 3. Which means that every three seconds, a new image is displayed.

The bottom two windows both belong to a single Carousel. Initially, that Carousel's interval is also set to 3. Each window displays a new image every three seconds, alternating. So, the pattern is:

1. The left window displays a new image
2. 1.5 seconds later, the right window displays a new image
3. Another 1.5 seconds later, the left window displays a new image
4. And so on...

When the interval is changed to one second, all windows in the Carousel speed up. Each window displays a new image every second, and because they are alternating, the Carousel as a whole is displaying a new image every 0.5 seconds. If the Carousel had a third window, each window would continue to display a new image every second, and the Carousel would be updating every 0.33 seconds.


Single-window Carousels
-----------------------
A single-window Carousel is the simplest type of Carousel. Carousels are designed to manage multiple windows at a time, but there is nothing stopping you from opening a single photoset as its own Carousel. Sometimes this is preferred, if you only have a single folder you want to view, or have a folder of images that you want to display separately from other Carousels.


Carousel Names
--------------
When you [save](saving-loading-carousels.md#saving-a-carousel) a Carousel, you must give it a name which will be displayed in the `Saved Carousels` menu. However, if you are building a new Carousel from scratch, it will be unnamed until you choose to save it.

Unnamed Carousels will be given a name generated from the folder name of each photoset, separated by a pipe (`|`).

For example, if you have added the following folders to a Carousel:

```
/path/to/images/circus
/path/to/images/Kaleidoscope
/path/to/images/carousel
```

The name displayed in the menu will be `circus | Kaleidoscope | carousel`.
