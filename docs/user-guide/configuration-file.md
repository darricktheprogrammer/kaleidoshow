Configuration File
==================
A yaml file can be used for hand-writing preferences instead of using the `Preferences` menu option. This is unlikely to be used, so it is remaining undocumented for now. If you would like to try it out, you can open the Preferences and save them. This will create a preferences file you can reference in your system's standard application data location:

* macOS - ~/Library/Application Support/Kaleidoshow
* Windows - %APPDATA%/Local Settings/Kaleidoshow
* Linux - ~/.local/share/Kaleidoshow


Modified preferences can be saved back to the same location. Kaleidoshow will need to be restarted to load the updated configuration.


XDG
---
On macOS and Linux, the config file can be located in `$XDG_CONFIG_HOME/Kaleidoshow` or any of the `$XDG_CONFIG_DIRS/Kaleidoshow` directories.

!!! Warning
	Be aware that if saving in an XDG_CONFIG location, values will be overwritten by values found in the main %APPDATA% location. This is because it is assumed that preferences changed within Kaleidoshow's UI are the most desired. For Kaleidoshow to properly read values from XDG_CONFIG, you may need to delete the config file in the main data location.
