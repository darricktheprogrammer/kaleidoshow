Controlling Kaleidoshow
=======================
Kaleidoshow provides several options for customizing the display of your photosets. While there are a handful of functions in the menu bar,  your main interaction with Kaleidoshow wll be by right-clicking on a window and choosing an option from the dropdown menu. Each action available in the right-click menu will apply to either the window under the cursor, or the Carousel to which it belongs.

![The context menu](../img/context-menu.png "The context menu")


Creating a New Carousel
-----------------------
The most familiar method of creating a new Carousel is through the `File` menu. Go to `File > New Carousel...` and you will be presented with a dialog box to choose a folder. Kaleidoshow will create a new single-window Carousel with images from the resulting photoset.

![New Carousel from menu](../img/new-carousel-menu.png "New Carousel from menu")

Alternatively, you can drag and drop a folder onto Kaleidoshow's icon in the dock (macOS) or taskbar (Windows/Linux).

![Drag on icon to open](../img/drag-open-icon.gif "Drag on icon to open")


Adding a Window to an Existing Carousel
---------------------------------------
You can add a new window to a Carousel by dragging and dropping a folder onto any of the Carousel's existing windows. The new window will be added to the end of the Carousel's window cycle. There is currently no way to change the window order within Kaleidoshow's UI. For more fine-grained control over Carousel creation, you can edit the [configuration file](configuration-file.md) or create a [Carousel File](carousel-files.md)

If the Carousel was saved previously, [reopening the Carousel](saving-loading-carousels.md#loading-a-carousel) will open the Carousel as it was saved. You must [resave](saving-loading-carousels.md#saving-a-carousel) the Carousel for the change to have a permanent effect.

As you can see below, the original image window is changing once per second. When the folder is dropped on the window, a second window appears and is added to the Carousel's cycle.

[image missing]


Removing a Window From a Carousel
---------------------------------
To remove a window from a Carousel, select the `Close` option from the bottom of the right-click menu.

If the Carousel was saved previously, [reopening the Carousel](saving-loading-carousels.md#loading-a-carousel) will open the Carousel as it was saved. You must [resave](saving-loading-carousels.md#saving-a-carousel) the Carousel for the change to have a permanent effect.


Closing a Carousel
------------------
Instead of closing windows one at at time, you can close all of the windows in a Carousel at one time in the menu bar by choosing <code>Active Carousels > Close: _CarouselName_</code>.


Starting/Stopping a Carousel
----------------------------
To stop a Carousel, choose the `Stop Carousel` option from the right-click menu. This will stop all of the windows in a Carousel without having to stop them individually.

![Stopping a Carousel](../img/carousel-stop.gif "Stopping a Carousel")


Starting/Stopping a Single Window
-----------------
You can stop a window by selecting the `Stop` option from the right-click menu. If the window is not the only window in a Carousel, the other windows will continue to display new images at the same rate as before. Only the stopped window will be affected.

You can restart a stopped window by selecting the `Start` option from the right-click menu.



Changing Carousel Speed
-----------------------
To change the speed of a Carousel, navigate to the `Interval` option in the right-click menu and choose any of the predefined intervals located there.

Additionally, at the bottom of the menu, you can choose `Custom...` and enter any interval you want. The value entered will become the speed, in seconds, for the Carousel. Custom intervals can be whole numbers or decimals between 0.1 and 999999.

The list of predefined speeds within the `Interval` menu can be [customized in preferences](preferences.md#interval-list).

![Interval menu](../img/interval-menu.png "Interval menu")


Showing Previous/Next Image
---------------------------
You can manually display a new image in a window by selecting either the `Next Image` or `Previous Image` option from the right-click menu. The window will then display the next or previous image in the photoset, respectively. Note that this will not change the timing of the Carousel. If you do it too closely to when the window is scheduled to display a new image, the image could be changed again quickly.

You can also use keyboard shortcuts to display the next image. Press `Ctrl + →` for the next image and `Ctrl + ←` for the previous image (`Cmd + →` and `Cmd + ←` on macOS).


Reordering a Photoset
---------------------
When a photoset is loaded, the order of the images depends on [your preferences](preferences.md#image-order) (or [Carousel file](carousel-files.md) if loading a saved Carousel). The Order submenu indicates the current order of the images with a checkmark and provides options to change it. When changed, the effect will take place when the next image is displayed.

Options in the Order submenu are:

* **Ordered -**
The images are shown in alphabetical order, ordered by their full filepath. When checked, choosing `Shuffled` will randomize the order of the images.

* **Shuffled -**
This is displayed when the images are Ordered. Clicking this will shuffle the images and they will begin to display in a randomized order.

* **Shuffled (click to reshuffle) -**
This is displayed when the images are already randomized. If you choose this option, the images will continue to be randomized, but in a different order than the current order.


Reloading a Photoset
--------------------
If images have been added or removed from a folder, select the `Refresh` option from the right-click menu to reload the photoset with the updated list of images.


Resizing a Window
-----------------
TODO


Other
-----

### Copy Functions
There are several options in the right-click menu for copying image filepath information to the clipboard.

Given the image `/path/to/image.jpg`, the following are example values that will be copied:

* **Copy path:** `/path/to/image.jpg`
* **Copy filename:** `image.jpg`
* **Copy basename:** `image`


### Tooltips
Hovering over a window will display a tooltip containing the following information about the currently displayed image:

* Carousel Name (or folder name for unsaved, single-window Carousels)
* Filename
* Photoset the image belongs to (helpful if the image is in a subfolder of the photoset)
* Full filepath to the image

![Tooltip Example](../img/tooltip.png "Tooltip Example")
