Preferences
===========
![Kaleidoshow preferences window](../img/prefs-window.png "Kaleidoshow preferences window")


Interval
--------
The default speed, in seconds, of newly opened Carousels. See [Changing Carousel Speed](controlling-kaleidoshow.md#changing-carousel-speed) for more details on valid values.


Interval menu options
---------------------
A list of pre-defined intervals for quickly changing Carousel speed in the [intervals menu](controlling-kaleidoshow.md#changing-carousel-speed). Each interval must be separated by a comma. See [Changing Carousel Speed](controlling-kaleidoshow#changing-carousel-speed) for more details on valid values.


Include Pattern
---------------
A [Python Regular Expression](https://docs.python.org/3/library/re.html). Only images whose path matches the pattern will be displayed.


Exclude Pattern
---------------
A [Python Regular Expression](https://docs.python.org/3/library/re.html). Images whose path matches the pattern will not be displayed.


Image Order
-----------
The [order](controlling-kaleidoshow.md#reordering-a-photoset) in which new photosets should be displayed.


Load images from subfolders
---------------------------
When checked, new photosets will be created from images in the top level of a given folder and all of its subfolders, recursively. The default is to only load images from the top level of the folder.


Start Carousels when opened
---------------------------
When checked, Carousels start begin cycling images as soon as they are opened. The default is to wait until they are started manually through the right-click menu.


Show Tooltips
-------------
When checked [image information](controlling-kaleidoshow.md#tooltips) will be shown when hovering over an image window. This is the default. Uncheck the `Show Tooltips` option if you do not want to see this information.
