User Guide
==========

* [Concepts and Terms](concepts.md)
* [Controlling Kaleidoshow](controlling-kaleidoshow.md)
* [Saving/Loading Carousels](saving-loading-carousels.md)
* [Preferences](preferences.md)
* [Configuration File](configuration-file.md)
* [Carousel Files](carousel-files.md)
